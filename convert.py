from __future__ import division, print_function
import json
import numpy as np
import os
import scipy.io
import scipy.misc
import shutil
import skimage.color
import skimage.io
import skimage.transform
import tempfile
import termcolor

from datasets import Hdf5Dataset, PartDataset


def copy_file(dirs, src_path, output_dir, category, item, take, grasp, view, ext):
    """Helper function for copying files with unified name."""

    # derive directory name
    dir_name = '%s_%03d_%03d_%03d' % (category, item, take, grasp)
    dir_path = os.path.join(output_dir, dir_name)

    # create destination directory if it does not exist
    if dir_name not in dirs:
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        dirs.add(dir_name)

    # copy file to the destination directory
    dst_path = os.path.join(output_dir, dir_path, '%s_%03d%s' % (dir_name, view, ext))
    shutil.copyfile(src_path, dst_path)
    print(dst_path)


def copy_doumanoglou(input_dir, output_dir):
    """Copy CERTH dataset of real training garments."""

    dirs = set()

    for category_name in os.listdir(input_dir):
        category = category_name.split('_')[0]
        for item_name in os.listdir(os.path.join(input_dir, category_name)):
            item = int(item_name.split('_')[1])
            for file_name in os.listdir(os.path.join(input_dir, category_name, item_name)):
                base, ext = os.path.splitext(file_name)
                if ext in ('.dps', '.jpg'):
                    parts = base.split('_')
                    grasp = int(parts[1])
                    take = int(parts[3])
                    view = int(parts[5])
                    copy_file(dirs, os.path.join(input_dir, category_name, item_name, file_name),
                              output_dir, category, item, take, grasp, view, ext)


def copy_certh(input_dir, output_dir):
    """Copy CERTH dataset of real training garments."""

    for input_name in os.listdir(input_dir):
        base, ext = os.path.splitext(input_name)
        parts = base.split('_')
        category = parts[0]
        grasp = int(parts[-3])
        item = int(parts[-2])
        take = int(parts[-1])
        output_name = '%s_%03d_%03d_%03d%s' % (category, item, take, grasp, ext)
        shutil.copyfile(os.path.join(input_dir, input_name), os.path.join(output_dir, output_name))


def copy_mariolis_train(input_dir, output_dir):
    """Copy CERTH dataset of real training garments."""

    dirs = set()

    for category in os.listdir(input_dir):
        for file_name in os.listdir(os.path.join(input_dir, category)):
            base, ext = os.path.splitext(file_name)
            parts = base.split('_')
            item = int(parts[0])
            take = 0
            grasp = int(parts[-2])
            view = int(parts[-1])

            copy_file(dirs, os.path.join(input_dir, category, file_name),
                      output_dir, category, item, take, grasp, view, ext)


def copy_mariolis_test(input_dir, output_dir):
    """Copy CERTH dataset of real testing garments."""

    dirs = set()

    for category in os.listdir(input_dir):
        for item_name in os.listdir(os.path.join(input_dir, category)):
            item = int(item_name)
            for take, data_dir in enumerate(os.listdir(os.path.join(input_dir, category, item_name))):
                for file_name in os.listdir(os.path.join(input_dir, category, item_name, data_dir)):
                    path = os.path.join(input_dir, category, item_name, data_dir, file_name)
                    base, ext = os.path.splitext(file_name)
                    if base.startswith('depth_img'):
                        view = int(base[9:])
                        copy_file(dirs, path, output_dir, category, item, take, 0, view, ext)
                    elif base.startswith('rgb_img'):
                        view = int(base[7:])
                        img = skimage.io.imread(path)
                        with tempfile.NamedTemporaryFile(suffix='.jpg', delete=False) as f:
                            skimage.io.imsave(f.name, img)
                            copy_file(dirs, f.name, output_dir, category, item, take, 0, view, '.jpg')
                    elif file_name == 'merged.pcd':
                        copy_file(dirs, path, output_dir, category, item, take, 0, 0, '.pcd')


def copy_mariolis_sim(input_dir, output_dir):
    """Copy CERTH dataset of simulated garments."""

    dirs = set()

    for category in os.listdir(input_dir):
        for item_name in os.listdir(os.path.join(input_dir, category)):
            for file_name in os.listdir(os.path.join(input_dir, category, item_name)):
                base, ext = os.path.splitext(file_name)
                parts = base.split('_')
                item = int(parts[1])
                take = int(parts[2]) * 100 + int(parts[3]) * 10 + int(parts[4])
                grasp = int(parts[5])
                view = int(parts[6])

                copy_file(dirs, os.path.join(input_dir, category, item_name, file_name),
                          output_dir, category, item, take, grasp, view, ext)


def depth_to_points(depth, mask, num_points):
    """Convert depth map to point cloud."""

    # grid of x and y coordinates of the point cloud
    h, w = depth.shape
    x, y = np.meshgrid(range(w), range(h))

    # keep only points from the mask
    x = x[mask]
    y = y[mask]
    z = depth[mask]
    n = len(x)

    # center x, y and set their step to 2 mm
    x = 0.002 * (x - w // 2)
    y = 0.002 * (y - h // 2)

    # convert z from meters to millimeters
    z = 0.001 * z

    if n < num_points:
        termcolor.cprint('Cloud contains only %d points.' % n, color='red')
        return None

    # sample points
    ind = np.random.choice(n, num_points, replace=False)
    pts = np.hstack((x[ind, None], y[ind, None], z[ind, None])).astype(np.float32)

    return pts


def convert_sun(input_dir, npz_path, num_points=1024):
    """Convert Glasgow dataset of depth maps stored as PNG images to NPZ point clouds."""

    # translation of category names to ids
    category_names = {'towel': 0, 'jean': 1, 't-shirt': 2, 'shirt': 3, 'thick-sweater': 4}

    points = list()
    categories = list()
    names = list()

    for item_name in sorted(os.listdir(input_dir)):
        item_dir = os.path.join(input_dir, item_name)

        info = scipy.io.loadmat(os.path.join(item_dir, 'info.mat'))
        category = category_names[info['category'][0]]

        take_names = sorted(os.listdir(item_dir))
        num_takes = len(take_names) // 3

        for take in range(num_takes):
            name = '%s_%03d_%03d' % (str(info['category'][0]), int(item_name), take + 1)
            print(name)

            depth = scipy.misc.imread(os.path.join(item_dir, take_names[3 * take]))
            mask = scipy.misc.imread(os.path.join(item_dir, take_names[3 * take + 1]))

            # resize the mask to fit the depth shape if needed
            if mask.shape != depth.shape:
                mask = skimage.transform.resize(mask, depth.shape,
                                                order=0, mode='reflect', preserve_range=True)
                mask = mask.astype(np.uint8)

            pts = depth_to_points(depth, mask == 2, num_points)

            if pts is not None:
                points.append(pts)
                categories.append(category)
                names.append(name)

    np.savez(npz_path, x=points, y=categories, names=names,
             classes=sorted(category_names, key=category_names.get))


def convert_corona(input_dir, npz_path, num_points=2048):
    """Convert IRI dataset of depth maps to NPZ point clouds."""

    points = list()
    categories = list()
    classes = list()

    for category, file_name in enumerate(os.listdir(input_dir)):
        classes.append(os.path.splitext(file_name)[0])

        depths = np.load(os.path.join(input_dir, file_name))

        for i, depth in enumerate(depths):
            print('%s: %d / %d' % (file_name, i + 1, len(depths)))

            pts = depth_to_points(depth, depth > 0, num_points)
            if pts is not None:
                points.append(pts)
                categories.append(category)

    np.savez(npz_path, x=points, y=categories, classes=classes)


def convert_parts(parts_path, npz_path):
    """Convert part format to single NPZ file."""

    data_train = PartDataset(parts_path, npoints=2048, classification=True)
    data_test = PartDataset(parts_path, npoints=2048, classification=True, train=False)
    data = list(data_train) + list(data_test)

    points = np.concatenate([d[0].numpy()[np.newaxis] for d in data])
    categories = np.concatenate([d[1].numpy() for d in data])
    splits = np.concatenate([np.full(len(data_train), 't'), np.full(len(data_test), 's')])

    np.savez(npz_path, x=points, y=categories, s=splits)


def convert_shapenet_split(json_path, txt_path):
    """Convert the ShapeNet split from JSON to text."""

    with open(json_path, mode='r') as f:
        data = json.load(f)

    with open(txt_path, mode='w') as f:
        for d in data:
            d = str(d).split('/')
            f.write(d[1] + '/' + d[2] + '\n')


def convert_shapenet(input_dir, npz_path, split_path=None, num_points=2048):
    """Convert the ShapeNet dataset to NPZ file."""

    points = list()
    categories = list()

    # names of categories included in dataset
    category_names = sorted(next(os.walk(input_dir))[1])

    # names of items included in the split
    if split_path is not None:
        split_items = set(line.strip() for line in open(split_path).readlines())

    for cat, cat_name in enumerate(category_names):
        print(cat_name)

        for item in sorted(os.listdir(os.path.join(input_dir, cat_name))):
            cat_item = cat_name + '/' + os.path.splitext(item)[0]
            print(cat_item)

            if split_path is None or cat_item in split_items:
                item_path = os.path.join(input_dir, cat_name, item)
                pts = np.loadtxt(item_path, dtype=np.float32)

                if pts.shape[0] >= num_points:
                    ind = np.random.choice(pts.shape[0], num_points, replace=False)
                    points.append(pts[ind])
                    categories.append(cat)

    np.savez(npz_path, x=points, y=categories, classes=category_names)


def convert_hdf5(file_list, classes_path, npz_path):
    """Convert HDF5 dataset to NPZ file."""

    data = Hdf5Dataset(file_list, classes_path, num_pts=0, normalize=False, normals=True)
    np.savez(npz_path, x=data.x, y=data.y, classes=data.classes)


def convert_pcd_binary_to_ascii(binary_dir, ascii_dir):
    """Convert binary PCD files to ascii format."""
    for pcd_file in os.listdir(binary_dir):
        binary_path = os.path.abspath(os.path.join(binary_dir, pcd_file))
        ascii_path = os.path.abspath(os.path.join(ascii_dir, pcd_file))
        os.system('pcl_convert_pcd_ascii_binary %s %s ascii' % (binary_path, ascii_path))


def load_pcd_file(file_path):
    """Load point cloud stored in text PCD format."""

    with open(file_path, mode='r') as f:
        num_pts = -1
        dtype = np.float32

        # read header
        while True:
            line = f.readline().strip().split(' ')
            if line[0] == 'TYPE':
                if line[1] in ('I', 'U'):
                    dtype = np.int32
                elif line[1] == 'F':
                    dtype = np.float32
            elif line[0] == 'POINTS':
                num_pts = int(line[1])
            elif line[0] == 'DATA':
                break

        # read point coordinates
        pts = np.fromfile(f, dtype=dtype, sep=' ')
        pts = pts.reshape([num_pts, 3])

        return pts


def merge_pcd(pcd_dir, npz_path, num_points=2048):
    """Merge PCD files to single NPZ file."""

    category_ids = dict()
    category_names = list()
    points = list()
    categories = list()
    names = list()

    # load point clouds
    for file_name in sorted(os.listdir(pcd_dir)):
        name, ext = os.path.splitext(file_name)

        if ext == '.pcd':
            print(name)

            pts = load_pcd_file(os.path.join(pcd_dir, file_name))

            if pts.shape[0] < num_points:
                termcolor.cprint('Point cloud contains %d points.' % pts.shape[0], 'red')

            else:
                # sample random subset of points
                if pts.shape[0] > num_points:
                    pts = pts[np.random.choice(pts.shape[0], num_points, replace=False)]

                # convert category name to id
                category = os.path.splitext(file_name)[0].split('_')[0]
                if category not in category_ids:
                    category_ids[category] = len(category_ids)
                    category_names.append(category)

                points.append(pts)
                categories.append(category_ids[category])
                names.append(name)

    np.savez(npz_path, x=points, y=categories, names=names, classes=category_names)


def filter_doumanoglou_lowest(input_path, output_path):
    """Filter CERTH data, keeping only the lowest grasping points."""

    LOWEST_GRASP = {1,     # shirts
                    2,     # trousers
                    3, 4,  # shorts 1
                    7, 8}  # t-shirts

    data = np.load(input_path)
    ind = list()

    for i, name in enumerate(data['names']):
        grasp = int(name.split('_')[3])
        if grasp in LOWEST_GRASP:
            ind.append(i)

    ind = np.array(ind)
    np.savez(output_path, x=data['x'][ind], y=data['y'][ind], f=data['f'][ind],
             names=data['names'][ind], classes=data['classes'])


def sample_dataset(input_path, output_path, num_data):
    data = np.load(input_path)
    num_classes = len(data['classes'])
    num_class_data = num_data // num_classes

    ind = list()
    for c in range(num_classes):
        ind_c = np.where(data['y'] == c)[0]
        ind_c = np.random.choice(ind_c, num_class_data, replace=False)
        ind.extend(ind_c)

    np.savez(output_path, x=data['x'][ind], y=data['y'][ind], classes=data['classes'])


def split_shapenet(input_path, train_path, test_path):
    """Split ShapeNet dataset to training and testing part."""

    data = np.load(input_path)
    x = data['x']
    y = data['y'].astype(np.int64)
    split = data['s'].astype(np.dtype('U1'))

    # use training (70%) and testing (20%) data for training, validation (10%) for testing
    train = np.logical_or(split == 't', split == 's')
    test = (split == 'v')

    # generic names of classes
    classes = [str(c) for c in range(np.max(y) + 1)]

    np.savez(train_path, x=x[train], y=y[train], classes=classes)
    np.savez(test_path, x=x[test], y=y[test], classes=classes)


if __name__ == '__main__':
    # copy_doumanoglou('data/doumanoglou', 'data/doumanoglou-rgbd')
    # copy_certh('data/certh-pcd-1', 'data/certh-pcd')

    # copy_mariolis_train('data/mariolis-train', 'data/mariolis-train-rgbd')
    # copy_mariolis_test('data/mariolis-test', 'data/mariolis-test-rgbd')
    # copy_mariolis_sim('data/mariolis-sim', 'data/mariolis-sim-rgbd')

    # convert_sun('data/sun', 'data/sun.npz')

    # convert_corona('data/corona-real', 'data/corona.npz')

    # convert_parts('data/shapenetcore', 'data/shapenetcore.npz')
    # convert_shapenet_split('data/shapenetcore_normal/shuffled_val_file_list.json',
    #                        'data/shapenetcore_normal/split_val.txt')
    # convert_shapenet('data/shapenetcore_normal', 'data/shapenetcore-normal.npz')
    # convert_hdf5('data/modelnet_hdf5/train_files.txt', 'data/modelnet_hdf5/shape_names.txt',
    #              'data/modelnet-normal-train.npz')

    # convert_pcd_binary_to_ascii('data/mariolis-test-pcd-bin', 'data/mariolis-test-pcd')
    # merge_pcd('data/mariolis-test-pcd', 'data/mariolis-test.npz')

    # filter_doumanoglou_lowest('data/doumanoglou.npz', 'data/doumanoglou-lowest.npz')

    # sample_dataset('data/mn40-n-r.npz', 'data/mn40-n-r-400.npz', 400)
    # sample_dataset('data/mn40-n-s.npz', 'data/mn40-n-s-80.npz', 80)

    split_shapenet('data/shapenet.npz', 'data/shapenet-r.npz', 'data/shapenet-s.npz')
