from __future__ import division
import progress.bar


class CountingMixin(object):
    count = 0

    def update_count(self):
        self.count += 1


class MetricMixin(object):
    metrics = dict()

    def add_metric(self, item, value=0):
        self.metrics[item] = value

    def update_metric(self, item, value):
        self.metrics[item] += value

    def __getattr__(self, item):
        if item.startswith('avg_'):
            value = self.metrics[item[4:]]
            return value / self.count if self.count > 0 else value
        else:
            return self.metrics[item]


class TimeMixin(object):
    @property
    def time(self):
        return self.eta_td if self.count < self.max else self.elapsed_td


class TimeBar(progress.bar.Bar, CountingMixin, TimeMixin):
    suffix = '%(index)d/%(max)d | %(time)s'

    def __init__(self, num_batches, title='Time'):
        super(TimeBar, self).__init__(title, max=num_batches)

    def next(self):
        self.update_count()
        super(TimeBar, self).next()


class EvaluationBar(progress.bar.Bar, CountingMixin, MetricMixin, TimeMixin):
    suffix = '%(index)d/%(max)d | %(time)s | %(avg_accuracy).3f'

    def __init__(self, num_batches, title='Eval'):
        super(EvaluationBar, self).__init__(title, max=num_batches)
        self.add_metric('accuracy')

    def next(self, accuracy):
        self.update_count()
        self.update_metric('accuracy', accuracy)
        super(EvaluationBar, self).next()


class TrainingBar(progress.bar.Bar, CountingMixin, MetricMixin, TimeMixin):
    suffix = '%(index)d/%(max)d | %(time)s | %(avg_loss).3f | %(avg_accuracy).3f'

    def __init__(self, epoch, num_epochs, num_batches, title='Epoch'):
        title = '%s %d/%d' % (title, epoch, num_epochs)
        super(TrainingBar, self).__init__(title, max=num_batches)
        self.add_metric('loss')
        self.add_metric('accuracy')

    def next(self, loss, accuracy):
        self.update_count()
        self.update_metric('loss', loss)
        self.update_metric('accuracy', accuracy)
        super(TrainingBar, self).next()
