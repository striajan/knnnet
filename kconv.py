from __future__ import print_function
import collections
import numpy as np
import torch
import torch.autograd
import torch.nn as nn
import torch.nn.parameter
import torch.nn.init as init

import tensors


class KnnConvFull(nn.Module):
    """Full convolution over k nearest neighbors using 1D convolution.

    :type in_channels: int
    :type out_channels: int
    :type k: int
    """

    def __init__(self, in_channels, out_channels, k, center=False, batch_processing=True):
        super(KnnConvFull, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.k = k
        self.center = center
        self.batch_processing = batch_processing

        self.ind = tensors.BatchIndex()
        self.conv = nn.Conv1d(k * in_channels, out_channels, kernel_size=1)

    def forward(self, x, knn):
        x_knn = self.build_x_knn(x, knn)

        # center data by subtracting the centroid from neighbors (not including centroid itself)
        if self.center:
            x_knn = x_knn[:, self.in_channels:] - x.repeat(1, self.k - 1, 1)
            x_knn = torch.cat((x, x_knn), dim=1)

        # apply 1D convolution
        x_knn = self.conv(x_knn)

        return x_knn

    def build_x_knn(self, x, knn):
        """Build tensor containing k neighbors of each point: (b, d, n) -> (b, k*d, n)."""

        b, _, n = x.shape

        if self.batch_processing:
            # auxiliary indices used for k nearest neighbors indexing
            ind = self.ind.get(x, n * self.k)

            # indices of k nearest neighbors
            knn = knn[:, :, :self.k].contiguous().view(b * n * self.k)

            # select the k neighboring points
            x_knn = x.transpose(2, 1)
            x_knn = x_knn[ind, knn].view(b, n, self.k * self.in_channels)
            x_knn = x_knn.transpose(1, 2)

        else:
            # preallocate tensor to hold k nearest neighbors of each point
            x_knn = x.new_empty((b, self.k * self.in_channels, n))

            # fill it for each item in batch
            for i in range(b):
                x_i = x[i].t()
                knn_i = knn[i, :, :self.k].contiguous().view(n * self.k)
                x_knn[i] = x_i[knn_i].view(self.k * self.in_channels, n)

        return x_knn

    def __repr__(self):
        return '{name}(in={in_channels}, out={out_channels}, k={k}, center={center})'.format(
            name=self.__class__.__name__, **self.__dict__)


class SumPool(nn.Module):
    """Pooling based on weighted sum over the specified dimensions.

    :type weight_size: tuple
    :type reduce_dim: int | tuple[int]
    """

    def __init__(self, weight_size, reduce_dim):
        super(SumPool, self).__init__()

        # shape of weights
        self.weight_size = tuple(weight_size)

        # dimensions to be summed over
        if not isinstance(reduce_dim, collections.abc.Iterable):
            reduce_dim = (reduce_dim,)
        self.reduce_dim = tuple(reduce_dim)

        # reduced shape of bias
        bias_size = weight_size
        for d in reversed(self.reduce_dim):
            bias_size = bias_size[:d] + bias_size[d + 1:]

        self.weight = torch.nn.parameter.Parameter(torch.FloatTensor(*self.weight_size))
        self.bias = torch.nn.parameter.Parameter(torch.FloatTensor(*bias_size))

        self.register_parameter('weight', self.weight)
        self.register_parameter('bias', self.bias)

    def forward(self, x):
        # weighted sum
        x *= self.weight

        # reduce dimensions by summation
        for d in reversed(self.reduce_dim):
            x = torch.sum(x, dim=d)

        # add bias
        x += self.bias

        return x

    def __repr__(self):
        return '{name}(size={weight_size}, dim={reduce_dim})'.format(
            name=self.__class__.__name__, **self.__dict__)


class SpatialPool(nn.Module):
    """Pooling based on spatial location of points.

    :type k: int
    :type num_channels: int
    :type num_dist: int
    :type num_angle: int
    """

    def __init__(self, k, num_channels, num_dist=10, num_angle=10, channel_wise=True, max_dist=0.05):
        super(SpatialPool, self).__init__()

        self.k = k
        self.num_channels = num_channels
        self.num_dist = num_dist
        self.num_angle = num_angle

        self.epsilon_angle = 1e-6
        self.alpha_dist = np.log(num_dist) / max_dist

        if channel_wise:
            weight_shape = (num_channels, num_dist, num_angle)
        else:
            weight_shape = (1, num_dist, num_angle)
        self.weight = torch.nn.Parameter(torch.FloatTensor(*weight_shape))
        self.register_parameter('weight', self.weight)

    def forward(self, x, pts, norm, dist, knn):
        b, d, _, n = x.shape

        # preallocate tensor of features pooled over k neighbors
        x_pooled = x.new_empty((b, d, n))

        # fill it for each item in batch
        for i in range(b):
            # repeat each of n points and normals k-times ~ (3, k, n)
            pts_k_i = pts[i].unsqueeze(1).expand(-1, self.k, -1)
            norm_k_i = norm[i].unsqueeze(1).expand(-1, self.k, -1)

            # select k neighbors of each point ~ (3, k, n)
            pts_i = pts[i].t()
            knn_i = knn[i, :, 1:self.k + 1].contiguous().view(n * self.k)
            pts_knn_i = pts_i[knn_i].view(n, self.k, 3)
            pts_knn_i = pts_knn_i.transpose(0, 2)

            # translation vectors to k neighbors of each point ~ (3, k, n)
            trans_i = pts_knn_i - pts_k_i

            # distances to k neighbors of each point ~ (k, n)
            dist_i = torch.sqrt(torch.sum(trans_i ** 2, dim=0))

            # dot products of translation vectors and normal vectors ~ (k, n)
            dot_i = torch.sum(trans_i * norm_k_i, dim=0)

            # angles between normals and translation vectors ~ (k, n)
            cos_i = dot_i / dist_i
            angle_i = torch.acos(cos_i)

            # convert distances to discrete indices ~ (k, n)
            ind_dist_i = (1 - torch.exp(-self.alpha_dist * dist_i)) * self.num_dist
            ind_dist_i = ind_dist_i.type(torch.long)
            ind_dist_i.clamp_(0, self.num_dist - 1)

            # convert angles to discrete indices ~ (k, n)
            ind_angle_i = angle_i * (self.num_dist / np.pi)
            ind_angle_i = ind_angle_i.type(torch.long)
            ind_angle_i.clamp_(0, self.num_angle - 1)

            # wighted sum pooling over k neighbors ~ (d, k, n) -> (d, n)
            weight_i = self.weight[:, ind_dist_i, ind_angle_i]
            x_pooled[i] = torch.sum(weight_i * x[i], dim=1)

        return x_pooled

    def __repr__(self):
        return ('{name}(k={k}, num_channels={num_channels}, num_dist={num_dist}, '
                'num_angle={num_angle})').format(name=self.__class__.__name__, **self.__dict__)


class KnnConvPair(nn.Module):
    """Pairwise convolution over k nearest neighbors using 2D convolution.

    :type in_channels: int
    :type out_channels: int
    :type k: int
    """

    def __init__(self, in_channels, out_channels, k):
        super(KnnConvPair, self).__init__()

        self.in_channels = in_channels
        self.out_channels = out_channels
        self.k = k

        self.conv = nn.Conv2d(2 * in_channels, out_channels, kernel_size=1)

    def forward(self, x, knn):
        x_knn = self.build_x_knn(x, knn)

        # apply 2D convolution
        x_knn = self.conv(x_knn)

        return x_knn

    def build_x_knn(self, x, knn):
        """Build tensor containing k neighbors of each point: (b, d, n) -> (b, 2*d, k, n)."""
        b, _, n = x.shape

        # preallocate tensor to hold k nearest neighbors of each point
        x_knn = x.new_empty((b, 2 * self.in_channels, self.k, n))

        # fill it for each item in batch
        for i in range(b):
            # repeat each of n points k-times ~ (d, k, n)
            x_k_i = x[i].unsqueeze(1).expand(-1, self.k, -1)

            # select k neighbors of each point ~ (d, k, n)
            x_i = x[i].t()
            knn_i = knn[i, :, 1:self.k + 1].contiguous().view(n * self.k)
            x_knn_i = x_i[knn_i].view(n, self.k, self.in_channels)
            x_knn_i = x_knn_i.transpose(0, 2)

            # concatenate k copies of each point with its k neighbors ~ (2 * d, k, n)
            x_knn[i, :self.in_channels] = x_k_i
            x_knn[i, self.in_channels:] = x_knn_i

        return x_knn

    def __repr__(self):
        return '{name}(in={in_channels}, out={out_channels}, k={k})'.format(
            name=self.__class__.__name__, **self.__dict__)


def main():
    d, n, k, c = 3, 4, 2, 5

    x = torch.tensor([[[1, 10, 100, 1000], [2, 20, 200, 2000], [3, 30, 300, 3000]]],
                     dtype=torch.float)
    knn = torch.tensor([[[0, 1, 2], [1, 2, 0], [2, 1, 3], [3, 2, 1]]],
                       dtype=torch.long)

    full = KnnConvFull(in_channels=d, out_channels=c, k=k + 1)
    init.constant_(full.conv.weight, 1)
    init.constant_(full.conv.bias, 0)
    x_full = full(x, knn)

    print('KnnConvFull:')
    print(x_full)

    pair = KnnConvPair(in_channels=d, out_channels=c, k=k)
    init.constant_(pair.conv.weight, 1)
    init.constant_(pair.conv.bias, 0)
    x_pair = pair(x, knn)

    print('KnnConvPair:')
    print(x_pair)

    conv_1 = nn.Conv2d(in_channels=c, out_channels=c, kernel_size=1)
    init.constant_(conv_1.weight, 1)
    init.constant_(conv_1.bias, 0)
    x_conv_1 = conv_1(x_pair).squeeze(dim=2)

    print('Conv2d-1:')
    print(x_conv_1)

    conv_k = nn.Conv2d(in_channels=c, out_channels=c, kernel_size=(k, 1), stride=(k, 1))
    init.constant_(conv_k.weight, 1)
    init.constant_(conv_k.bias, 0)
    x_conv_k = conv_k(x_pair).squeeze(dim=2)

    print('Conv2d-k:')
    print(x_conv_k)

    max = nn.MaxPool2d(kernel_size=(k, 1))
    x_max = max(x_pair).squeeze(dim=2)

    print('MaxPool2d:')
    print(x_max)

    avg = nn.AvgPool2d(kernel_size=(k, 1))
    x_avg = avg(x_pair).squeeze(dim=2)

    print('AvgPool2d:')
    print(x_avg)

    sum = SumPool(weight_size=(1, c, k, 1), reduce_dim=2)
    init.constant_(sum.weight, 1)
    init.constant_(sum.bias, 0)
    x_sum = sum(x_pair).squeeze(dim=2)

    print('SumPool:')
    print(x_sum)


if __name__ == '__main__':
    main()
