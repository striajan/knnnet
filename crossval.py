from __future__ import print_function
import math
import numpy as np
import scipy.stats


class LogUniform(object):
    """Generator of random values on logarithmic scale from low to high."""

    def __init__(self, low, high, base=np.e):
        super(LogUniform, self).__init__()

        self.base = base
        self.low = math.log(low, base)
        self.high = math.log(high, base)

    def rvs(self, random_state=None):
        x = scipy.stats.uniform.rvs(loc=self.low, scale=self.high - self.low,
                                    random_state=random_state)
        return np.power(self.base, x)


def log_space(low, high, num, base=np.e):
    """Generate values on logarithmic scale from low to high."""
    low = math.log(low, base)
    high = math.log(high, base)
    return np.power(base, np.linspace(low, high, num))


def main():
    distr = LogUniform(1e-3, 1e-1, 10)
    print([distr.rvs() for _ in range(3)])

    print(log_space(1, np.e ** 2, 5))
    print(log_space(1e-5, 1e-1, 5, base=10))


if __name__ == '__main__':
    main()
