from __future__ import print_function
import datetime
import functools
import gc
import numpy as np
import os
import pickle
import random
import shutil
import sys
from tabulate import tabulate
import torch


def build_output_dir(base_path, model):
    """Build path for writing output."""

    if __debug__:
        out_path = os.path.join(base_path, 'tmp')

        # clean temporary directory
        if os.path.exists(out_path):
            shutil.rmtree(out_path, ignore_errors=True)

    else:
        out_path = list()

        # date and time
        out_path.append(datetime.datetime.now().strftime('%m%d-%H%M'))

        # process id
        out_path.append(str(os.getpid()))

        # optional model summary
        if hasattr(model, 'summary'):
            out_path.append(model.summary())

        out_path = os.path.join(base_path, '_'.join(out_path))

    os.makedirs(out_path, exist_ok=True)

    return out_path


def count_params(model, grad_only=True):
    """Count parameters of the model (all of them or only those requiring gradient).

    :type model: torch.nn.Module
    :type grad_only: bool
    :rtype: str
    """

    table = list()
    total_count = 0

    for name, par in model.named_parameters():
        if not grad_only or par.requires_grad:
            shape = par.data.cpu().numpy().shape
            count = np.prod(shape)
            total_count += count
            table.append((name, shape, count))

    table.append(('Total', '', total_count))
    header = ('Name', 'Shape', 'Count')

    return str(tabulate(table, header, tablefmt='plain'))


def set_gpu(gpu_id=0, overwrite=False):
    """Set environment variable to use the specified GPU.

    :type gpu_id: int
    :type overwrite: bool
    """
    if 'CUDA_VISIBLE_DEVICES' not in os.environ or overwrite:
        os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu_id)


def backup_sources(backup_dir, source_dir='.', extension='.py'):
    """Copy all Python sources from to the backup directory."""

    if not os.path.exists(backup_dir):
        os.makedirs(backup_dir)

    for file_name in os.listdir(source_dir):
        if extension == os.path.splitext(file_name)[1]:
            shutil.copy(os.path.join(source_dir, file_name), backup_dir)


def save_params(output_dir, params):
    """Save parameters of the model.

    :type output_dir: str
    :type params: KnnNetParams
    """
    with open(os.path.join(output_dir, 'params.pkl'), mode='wb') as f:
        pickle.dump(params, f, protocol=pickle.DEFAULT_PROTOCOL)


def load_params(file_path):
    """Load parameters of the model.

    :type file_path: str
    :rtype: KnnNetParams
    """
    with open(file_path, mode='rb') as f:
        return pickle.load(f)


class ModelSaver:
    """Wrapper for model state saving."""

    def __init__(self, output_dir, model, optimizer=None, lr_schedule=None, num_epochs=0, frequency=1):
        self.output_dir = os.path.join(output_dir, 'model')
        os.makedirs(self.output_dir)
        self.model = model
        self.optimizer = optimizer
        self.lr_schedule = lr_schedule
        self.num_epochs = num_epochs
        self.frequency = frequency

    def save(self, epoch):
        # save model weights as often as required and in the last epoch
        if epoch == self.num_epochs or epoch % self.frequency == 0:
            state = self.model.state_dict()
            torch.save(state, os.path.join(self.output_dir, 'model_%03d.pth' % epoch))

        # serialize optimizer and LR schedule in the last epoch
        if epoch == self.num_epochs:
            torch.save(self.optimizer, os.path.join(self.output_dir, 'optim_%03d.pth' % epoch))
            torch.save(self.lr_schedule, os.path.join(self.output_dir, 'lr_%03d.pth' % epoch))


def data_summary(y_train, y_test=None, classes=None):
    """Summary table for training and testing dataset.

    :type y_train: numpy.ndarray
    :type y_test: numpy.ndarray
    :type classes: numpy.ndarray
    :rtype: str
    """

    header = list()
    table = list()
    summary = ['Overall']

    if classes is not None:
        header.append('Name')
        table.append(list(classes))
        summary.append('')

    header.append('Id')
    table.append(range(np.max(y_train) + 1))

    header.append('Train')
    table.append(np.bincount(y_train))
    summary.append(len(y_train))

    if y_test is not None:
        header.append('Test')
        table.append(np.bincount(y_test))
        summary.append(len(y_test))

    # transpose columns to rows
    table = list(zip(*table))

    table.append(summary)

    return str(tabulate(table, header, tablefmt='plain'))


def gpu_memory_usage():
    """Examine memory usage for each GPU used by the current process.

    :rtype: str
    """

    import gpustat

    memory_usage = []
    pid = os.getpid()

    try:
        for gpu in gpustat.GPUStatCollection.new_query():
            for process in gpu.processes:
                if pid == process['pid']:
                    memory_usage.append('GPU %d: %d / %d / %d' % (
                        gpu.index, process['gpu_memory_usage'], gpu.memory_used, gpu.memory_total))
    finally:
        return '\n'.join(memory_usage)


def trace_memory(scope='torch'):
    """Trace objects in memory aggregated by type. Allowed scope: 'tensor', 'torch', 'all'

    :type scope: str
    :rtype: str
    """

    mem = dict()
    count = size = 0

    for obj in gc.get_objects():
        obj_type = type(obj)

        # process either only tensors, torch objects or all objects
        process = True
        is_tensor = False
        if scope == 'tensor':
            process = torch.is_tensor(obj)
            is_tensor = process
        elif scope == 'torch':
            process = obj_type.__module__.split('.')[0] == 'torch'

        if process:
            if obj_type not in mem:
                # pretty module and class name
                name = obj_type.__module__ + '.' + obj_type.__name__
                mem[obj_type] = {'name': name, 'count': 0, 'size': 0}

            # count another object of the specified type
            mem[obj_type]['count'] += 1
            count += 1

            # estimate size of the object
            s = np.prod(obj.size()) if is_tensor and hasattr(obj, 'size') else sys.getsizeof(obj)
            mem[obj_type]['size'] += s
            size += s

    # sort by count of objects in descending order
    mem = sorted(mem.values(), key=lambda m: m['count'], reverse=True)

    # add summary
    mem.append({'name': 'total', 'count': count, 'size': size})

    return str(tabulate(mem, headers='keys', tablefmt='plain'))


class GradientHistogram(object):
    """Represents class for accumulating gradient of histograms."""
    def __init__(self, name, num_bins=100, low=-1.0, high=1.0):
        self.name = name
        self.num_bins = num_bins
        self.low = low
        self.high = high
        self.hist = torch.LongTensor(num_bins + 2)
        self.reset()

    def reset(self):
        """Reset the histogram by setting it to zero."""
        self.hist.fill_(0)

    def accumulate(self, grad):
        """Accumulate gradients to histogram.

        :type grad: torch.Tensor
        """
        grad = grad.data.cpu()

        hist = torch.histc(grad, self.num_bins, self.low, self.high)
        self.hist[1:self.num_bins + 1] += hist.long()

        self.hist[0] += (grad < self.low).long().sum()
        self.hist[-1] += (grad > self.high).long().sum()

    @property
    def bins(self):
        bins = np.linspace(self.low, self.high, self.num_bins + 1)
        return np.concatenate((bins, [np.Inf]))

    @property
    def frequencies(self):
        return self.hist.numpy()

    def __repr__(self):
        return '%s (bins=%d, low=%f, high=%f):\n%s' % (
            self.name, self.num_bins, self.low, self.high, self.frequencies)


def random_seed(seed=0):
    """Set random seed for Python, NumPy and PyTorch."""
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)


def partial(cls, *args, **kwargs):
    class ClsWrapper(cls):
        __init__ = functools.partialmethod(cls.__init__, *args, **kwargs)

    return ClsWrapper


def lr_schedule_params(lr_schedule):
    """Get filtered parameters of LR schedule.

    :type lr_schedule: torch.optim.lr_scheduler._LRScheduler
    :rtype: dict
    """
    filtered_keys = ('is_better', 'optimizer')
    return {key: val for key, val in lr_schedule.__dict__.items() if key not in filtered_keys}
