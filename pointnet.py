from __future__ import print_function
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.parallel
import torch.utils.data

from transnet import TransMat, TransNet, transform


class FeatNetSimple(nn.Module):
    def __init__(self, num_points=1024, global_feat=True):
        super(FeatNetSimple, self).__init__()

        self.num_points = num_points
        self.global_feat = global_feat
        self.trans = TransNet(num_points=num_points, point_dim=3)

        self.conv1 = torch.nn.Conv1d(3, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, 128, 1)
        self.conv3 = torch.nn.Conv1d(128, 1024, 1)
        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(128)
        self.bn3 = nn.BatchNorm1d(1024)
        self.mp1 = torch.nn.MaxPool1d(num_points)

    def forward(self, x):
        # estimate 3*3 transformation matrix and transform 3*n points
        t = self.trans(x)
        x = transform(t, x)

        # 3*n -> 64*n -> 128*n -> 1024*n
        f = x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = self.bn3(self.conv3(x))

        # max-pool over n points -> 1024 global features
        x = self.mp1(x)
        x = x.view(-1, 1024)

        if self.global_feat:
            # 1024 global features, 3*3 transformation
            return x, t
        else:
            # (1024+64)*n global and point features, 3*3 transformation
            x = x.view(-1, 1024, 1).repeat(1, 1, self.num_points)
            return torch.cat([x, f], 1), t


class FeatNetFull(nn.Module):
    def __init__(self, num_points=1024, global_feat=True):
        super(FeatNetFull, self).__init__()

        self.num_points = num_points
        self.global_feat = global_feat
        self.trans1 = TransNet(num_points=num_points, point_dim=3)
        self.trans2 = TransNet(num_points=num_points, point_dim=64)

        self.conv1 = torch.nn.Conv1d(3, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, 64, 1)
        self.conv3 = torch.nn.Conv1d(64, 64, 1)
        self.conv4 = torch.nn.Conv1d(64, 128, 1)
        self.conv5 = torch.nn.Conv1d(128, 1024, 1)
        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(64)
        self.bn3 = nn.BatchNorm1d(64)
        self.bn4 = nn.BatchNorm1d(128)
        self.bn5 = nn.BatchNorm1d(1024)

        self.mp1 = nn.MaxPool1d(num_points)

    def forward(self, x):
        # estimate 3*3 transformation matrix and transform 3*n points
        t1 = self.trans1(x)
        x = transform(t1, x)

        # 3*n -> 64*n -> 64*n
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))

        # estimate 64*64 transformation matrix and transform 64*n point features
        t2 = self.trans2(x)
        x = transform(t2, x)

        # 64*n -> 64*n -> 128*n -> 1024*n
        x = F.relu(self.bn3(self.conv3(x)))
        x = F.relu(self.bn4(self.conv4(x)))
        x = F.relu(self.bn5(self.conv5(x)))

        # max-pool over n point features -> 1024 global features
        x = self.mp1(x)
        x = x.view(-1, 1024)

        return x, t1


class PointNetCls(nn.Module):
    def __init__(self, num_points=1024, k=2, feat=None):
        super(PointNetCls, self).__init__()

        if feat is None:
            self.num_points = num_points
            self.feat = FeatNetSimple(num_points, global_feat=True)
            # self.feat = FeatNetFull(num_points, global_feat=True)
        else:
            self.num_points = feat.num_points
            self.feat = feat

        self.fc1 = nn.Linear(1024, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, k)
        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(256)
        # self.drop1 = nn.Dropout(p=0.7)
        # self.drop2 = nn.Dropout(p=0.7)
        self.relu = nn.ReLU()

    def forward(self, x):
        # compute 1024 global features and transformation
        f, t = self.feat(x)
        x = f

        # 1024 -> 512 -> 256 -> k
        x = F.relu(self.bn1(self.fc1(x)))
        # x = F.relu(self.drop1(self.fc1(x)))
        x = F.relu(self.bn2(self.fc2(x)))
        # x = F.relu(self.drop2(self.fc2(x)))
        x = F.log_softmax(self.fc3(x), dim=1)

        return x, t, f


class PointNetSeg(nn.Module):
    def __init__(self, num_points=1024, k=2):
        super(PointNetSeg, self).__init__()

        self.num_points = num_points
        self.k = k
        self.feat = FeatNetSimple(num_points, global_feat=False)

        self.conv1 = torch.nn.Conv1d(1088, 512, 1)
        self.conv2 = torch.nn.Conv1d(512, 256, 1)
        self.conv3 = torch.nn.Conv1d(256, 128, 1)
        self.conv4 = torch.nn.Conv1d(128, self.k, 1)
        self.bn1 = nn.BatchNorm1d(512)
        self.bn2 = nn.BatchNorm1d(256)
        self.bn3 = nn.BatchNorm1d(128)

    def forward(self, x):
        batch_size = x.size()[0]

        # compute (1024+64)*n global and point features and transformation
        x, t = self.feat(x)

        # (64+1024)*n -> 512*n -> 256*n -> 128*n -> k*n
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))
        x = self.conv4(x)

        # soft-max classification for each of n points
        x = x.transpose(2, 1).contiguous()
        x = F.log_softmax(x.view(-1, self.k), dim=1)
        x = x.view(batch_size, self.num_points, self.k)

        return x, t


def main():
    b, n, d, k = 4, 1024, 3, 10

    x = torch.rand(b, d, n)
    y = torch.randint(k, size=(b,), dtype=torch.long)

    feat = FeatNetSimple(n, global_feat=True)
    f = feat(x)[0]
    print('FeatNetSimple - global', f.size())

    feat = FeatNetSimple(n, global_feat=False)
    f = feat(x)[0]
    print('FeatNetSimple - local', f.size())

    cls = PointNetCls(n, k=k)
    f = cls(x)[0]
    print('PointNetCls', f.size())

    loss = torch.nn.functional.nll_loss(f, y)
    loss.backward()
    print('Loss:', loss.item())

    seg = PointNetSeg(n, k=k)
    f = seg(x)[0]
    print('PointNetSeg', f.size())


if __name__ == '__main__':
    main()
