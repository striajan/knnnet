from __future__ import print_function
import numpy as np
import scipy.spatial
import torch
import torch.nn as nn


class Distance(nn.Module):
    """Module for computing pairwise spatial distance of points."""

    def __init__(self):
        super(Distance, self).__init__()

    def forward(self, x):
        # compute squared pairwise distance of points in batches;
        # the computation is based on:
        # dist[n,i,j] = ||x[n,i] - x[n,j]||^2
        #             = ||x[n,i]||^2 - 2 * <x[n,i]|x[n,j]> + ||x[n,j]||^2
        norm = (x ** 2).sum(dim=1)
        dot = torch.bmm(x.transpose(2, 1), x)
        dist = norm.unsqueeze(dim=2) - 2 * dot + norm.unsqueeze(dim=1)

        return dist

    def __repr__(self):
        return '{name}()'.format(name=self.__class__.__name__)


def find_neighbors_kdtree(x, k):
    """Find approximate k nearest neighbors of all points using k-d trees.

    :type x: numpy.ndarray
    :type k: int
    """

    b, n, _ = x.shape
    knn = np.zeros([b, n, k], dtype=int)

    for i in range(b):
        tree = scipy.spatial.cKDTree(x[i])
        knn[i] = tree.query(x[i], k)[1]

    return knn


def find_neighbors_exact(x, k):
    """Find exact k nearest neighbors of all points.

    :type x: numpy.ndarray
    :type k: int
    """

    b, n, _ = x.shape
    knn = np.zeros([b, n, k], dtype=int)

    for i in range(b):
        d = scipy.spatial.distance_matrix(x[i], x[i])
        d[np.diag_indices(n, 2)] = -1
        knn[i] = np.argsort(d, axis=1)[:, :k]

    return knn


class KnnSearch(nn.Module):
    """Module for k nearest neighbors search.

    :type k: int
    """

    def __init__(self, k=-1):
        super(KnnSearch, self).__init__()

        self.k = k

    def forward(self, dist):
        # find k nearest neighbors of each point
        if self.k == -1:
            dist, knn = dist.sort(dim=2)
        else:
            dist, knn = dist.topk(self.k, dim=2, largest=False, sorted=True)

        return knn, dist

    def __repr__(self):
        return '{name}(k={k})'.format(name=self.__class__.__name__, **self.__dict__)


def main():
    b, d, n, k = 4, 3, 1024, 8
    x = torch.rand(b, d, n)

    distance = Distance()
    dist = distance(x.data)
    print('SpatialDistance', dist.size())

    search = KnnSearch(k)
    knn, dist_knn = search(dist)
    print('KnnSearch', knn.size(), dist_knn.size())


if __name__ == '__main__':
    main()
