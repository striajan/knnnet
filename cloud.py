from __future__ import division, print_function
import numpy as np
import os
import sklearn.linear_model

import plot_utils as pltu


def generate_circle_3d():
    """Generate points on random circle in 3D."""

    # parameters
    center = np.array([3, 2, 1])
    radius = 5
    u = np.array([1, 1, 0]) / np.sqrt(2)
    v = np.array([0, 0, 1])
    n = 100
    jitter = np.random.normal(scale=radius / 30, size=(n, 3))

    # generate
    phi = np.linspace(0, 2 * np.pi, n)
    u = np.cos(phi)[:, np.newaxis] * u[np.newaxis]
    v = np.sin(phi)[:, np.newaxis] * v[np.newaxis]
    circle = center + radius * (u + v) + jitter

    return circle


def check_plane_3d(xyz, inlier_threshold=0.8, dist_threshold=1.0):
    """Fit plane y = a * x + b * z + c by RANSAC regression and check
    whether the ratio of inliers is above a specified threshold and
    there are no points too far away from the plane."""

    xz = xyz[:, [0, 2]]
    y = xyz[:, 1]

    # RANSAC fitting
    reg = sklearn.linear_model.RANSACRegressor()
    reg.fit(xz, y)

    # ratio of fitted inliers to all points
    inlier_ratio = np.count_nonzero(reg.inlier_mask_) / len(y)

    # distance of point to plane
    dist = np.abs(reg.estimator_.predict(xz) - y)
    max_dist = np.max(dist)

    return (inlier_ratio > inlier_threshold) and (max_dist < dist_threshold)


def check_poses_planar(data_dir):
    """Check whether estimated sequences of camera poses form planes."""

    for file_name in os.listdir(data_dir):
        base, ext = os.path.splitext(file_name)

        if ext == '.txt':
            poses = np.loadtxt(os.path.join(data_dir, file_name))
            trans = poses[:, :3]
            is_planar = check_plane_3d(trans)

            print(base, is_planar)

            pltu.figure(base)
            pltu.scatter(*trans.transpose(), color='blue' if is_planar else 'red')
            pltu.axis_equal_3d()
            pltu.show()


if __name__ == '__main__':
    check_poses_planar('data/doumanoglou-pcd')
