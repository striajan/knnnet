from __future__ import print_function


class LayerParams(object):
    def __init__(self, size, batch_norm, activation):
        super(LayerParams, self).__init__()
        self.size = size
        self.batch_norm = batch_norm
        self.activation = activation


class ConvLayerParams(LayerParams):
    def __init__(self, m, k, size, batch_norm, activation, points, normals):
        super(ConvLayerParams, self).__init__(size, batch_norm, activation)
        self.m = m
        self.k = k
        self.points = points
        self.normals = normals


class FullConvLayerParams(ConvLayerParams):
    def __init__(self, m, k, size, center=False, batch_norm=True, activation='relu',
                 points=False, normals=False):
        super(FullConvLayerParams, self).__init__(m, k, size, batch_norm, activation,
                                                  points, normals)
        self.center = center


class PairConvLayerParams(ConvLayerParams):
    def __init__(self, m, k, size, pool_type='', batch_norm=True, activation='relu',
                 points=False, normals=False):
        super(PairConvLayerParams, self).__init__(m, k, size, batch_norm, activation,
                                                  points, normals)
        self.pool_type = pool_type


class LinearLayerParams(LayerParams):
    def __init__(self, size, drop=0.0, batch_norm=False, activation='relu'):
        super(LinearLayerParams, self).__init__(size, batch_norm, activation)
        self.drop = drop


class KnnNetParams:
    def __init__(self, num_pts=1024, num_cls=40, conv_type='full', input_dim=3):
        # size of the input point cloud
        self.num_pts = num_pts

        # number of predicted classes
        self.num_cls = num_cls

        # which convolution layers to use
        self.conv_type = conv_type

        # dimension of the input points
        self.input_dim = input_dim

        # aggregator of point-wise features
        self.aggregator = 'pool'

        self.conv = self.lin = None

        if conv_type == 'full':
            F = FullConvLayerParams
            self.set_conv((F(  0,  4,  32),
                           F(  0,  0,  64),
                           F(512,  8,  64),
                           F(  0,  0, 128),
                           F(256, 16, 128),
                           F(  0,  0, 256)))

            L = LinearLayerParams
            self.set_lin((L(    256, 0.3),
                          L(    128, 0.2),
                          L(num_cls, 0.1, activation='logsoftmax')))

        elif conv_type == 'pair':
            P = PairConvLayerParams
            self.set_conv((P(  0,  4,  64),
                           P(  0,  0, 128, 'max'),
                           P(512,  8, 128),
                           P(  0,  0, 256, 'max'),
                           P(256, 16, 256),
                           P(  0,  0, 512, 'max')))

            L = LinearLayerParams
            self.set_lin((L(    512, 0.5),
                          L(    256, 0.3),
                          L(num_cls, 0.1, activation='logsoftmax')))

    def set_conv(self, conv):
        self.conv = []
        for pars in conv:
            if self.conv_type == 'full' and not isinstance(pars, FullConvLayerParams):
                pars = FullConvLayerParams(*pars)
            elif self.conv_type == 'pair' and not isinstance(pars, PairConvLayerParams):
                pars = PairConvLayerParams(*pars)
            self.conv.append(pars)

    def set_lin(self, lin):
        self.lin = []
        for pars in lin:
            if not isinstance(pars, LinearLayerParams):
                pars = LinearLayerParams(*pars)
            self.lin.append(pars)

    def __repr__(self):
        s = 'k' + self.conv_type
        s += '-k:' + ':'.join(str(k) for k in self.k_conv if k != 0)
        s += '-c:' + ':'.join(str(c.size) for c in self.conv)
        s += '-a:' + self.aggregator[0]
        s += '-l:' + ':'.join(str(l.size) for l in self.lin)
        return s

    def set_conv_param(self, name, val):
        if len(val) != len(self.conv):
            raise ValueError('Incompatible number of %s (%s) and convolution layers (%s).' %
                             (name, len(val), len(self.conv)))
        for i in range(len(val)):
            setattr(self.conv[i], name, val[i])

    def set_lin_param(self, name, val):
        if len(val) not in (len(self.lin), len(self.lin) - 1):
            raise ValueError('Incompatible number of %s (%s) and linear layers (%s).' %
                             (name, len(val), len(self.conv)))
        for i in range(len(val)):
            setattr(self.lin[i], name, val[i])

    def set_m(self, m):
        self.set_conv_param('m', m)

    def set_k(self, k):
        self.set_conv_param('k', k)

    def set_c(self, c):
        self.set_conv_param('size', c)

    def set_l(self, l):
        self.set_lin_param('size', l)

    def set_drop(self, drop):
        self.set_lin_param('drop', drop)

    @property
    def m(self):
        return [p.m for p in self.conv]

    @property
    def k_conv(self):
        return [p.k for p in self.conv]

    @property
    def pool_type(self):
        return [p.pool_type for p in self.conv]
