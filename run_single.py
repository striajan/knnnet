from __future__ import print_function
import argparse
import torch

import augmentation
import knet
import kpars
import training


parser = argparse.ArgumentParser()
parser.add_argument('--batch_size', type=int, default=32, help='input batch size')
parser.add_argument('--num_points', type=int, default=1024, help='number of points in the cloud')
parser.add_argument('--num_epochs', type=int, default=100, help='number of epochs to train for')
parser.add_argument('--path_train', type=str, default='data/mn40-n-r.npz')
parser.add_argument('--path_test', type=str, default='data/mn40-n-s.npz')
parser.add_argument('--device', type=str, default='cuda', help='cpu or cuda')
parser.add_argument('--test_step', type=int, default=1, help='testing step in epochs')
parser.add_argument('--normalize', type=bool, default=True, help='normalize input points')
parser.add_argument('--normals', type=bool, default=False, help='load points normals')
parser.add_argument('--augment', type=bool, default=True, help='do augment training data')
parser.add_argument('--seed', type=int, default=1, help='seed for random generator')
parser.add_argument('--lr', type=float, default=1e-3, help='initial learning rate')
opt = parser.parse_args()

# model
params = kpars.KnnNetParams(opt.num_points, conv_type='full', input_dim=6 if opt.normals else 3)
model = knet.KnnNet(params).to(opt.device)

# optimizer
optimizer = torch.optim.Adam(model.parameters(), lr=opt.lr)

# learning rate
# lr_schedule = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer)
lr_schedule = torch.optim.lr_scheduler.StepLR(optimizer, step_size=20, gamma=0.7)

# augmentation
augment = augmentation.Composed(
    augmentation.Sampling(opt.num_points),
    augmentation.RotationY())

trainer = training.Trainer(
    params, model, optimizer, lr_schedule, opt.num_epochs, opt.batch_size, opt.path_train, opt.path_test,
    opt.num_points, opt.normalize, opt.normals, augment, opt.device, opt.seed, opt.test_step)
trainer.train()
