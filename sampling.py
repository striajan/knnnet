from __future__ import print_function
from enum import Enum
import math
import matplotlib.pyplot as plt
import numpy as np
import torch.autograd
import torch.distributions
import torch.nn as nn
import torch.nn.init as init
import torch.optim
import torch.random

import spatial
import plot_utils as pltu
import tensors
import utils


class Sampling(object):
    """Module for point-wise sampling."""

    def __init__(self, batch_indexing=False):
        super(Sampling, self).__init__()

        self.batch_indexing = batch_indexing

        self.ind = tensors.BatchIndex()

    @staticmethod
    def sample_bdn_batch(bdn, sample, index):
        d = bdn.shape[1]
        b, m = sample.shape

        ind = index.get(bdn, m)
        sample = sample.view(b * m)

        bdm = bdn.transpose(2, 1)
        bdm = bdm[ind, sample].view(b, m, d)
        bdm = bdm.transpose(1, 2)

        return bdm

    @staticmethod
    def sample_bdn_loop(bdn, sample):
        d = bdn.shape[1]
        b, m = sample.shape

        bdm = bdn.new_empty((b, d, m))
        for i in range(b):
            bdm[i] = bdn[i][:, sample[i]]

        return bdm

    def sample_bdn(self, bdn, sample):
        """Sample b*d*m tensor from b*d*n data in point-wise manner.

        :type bdm: torch.Tensor
        :type sample: torch.Tensor
        :rtype: torch.Tensor
        """
        return (Sampling.sample_bdn_batch(bdn, sample, self.ind) if self.batch_indexing else
                Sampling.sample_bdn_loop(bdn, sample))

    @staticmethod
    def sample_bnn_batch(bnn, sample, index):
        n = bnn.shape[1]
        b, m = sample.shape

        ind = index.get(bnn, m)
        sample = sample.view(b * m)

        bmm = bnn[ind, sample].view(b, m, n)
        bmm = bmm.transpose(2, 1)
        bmm = bmm[ind, sample].view(b, m, m)
        bmm = bmm.transpose(1, 2)

        return bmm

    @staticmethod
    def sample_bnn_loop(bnn, sample):
        b, m = sample.shape

        bmm = bnn.new_empty((b, m, m))
        for i in range(b):
            bmm[i] = bnn[i][sample[i]][:, sample[i]]

        return bmm

    def sample_bnn(self, bnn, sample):
        """Sample b*m*m tensor from b*n*n data in point-wise manner.

        :type bnn: torch.Tensor
        :type sample: torch.Tensor
        :rtype: torch.Tensor
        """
        return (Sampling.sample_bnn_batch(bnn, sample, self.ind) if self.batch_indexing else
                Sampling.sample_bnn_loop(bnn, sample))


class RandomSampling(nn.Module):
    """Module for random sampling of points.

    :type m: int
    :type batch_wise: bool
    """

    def __init__(self, m, batch_wise=True):
        super(RandomSampling, self).__init__()

        self.m = m
        self.batch_wise = batch_wise

        self.sampling = Sampling()

    def forward(self, x, pts, norm, dist):
        b, _, n = x.shape

        # sample all data items in batch using same indices
        if self.batch_wise:
            perm = torch.randperm(n)[:self.m].to(x.device)
            x = x[:, :, perm]
            if pts is not None:
                pts = pts[:, :, perm]
            if norm is not None:
                norm = norm[:, :, perm]
            if dist is not None:
                dist = dist[:, perm][:, :, perm]

        # sample each item in batch using different indices
        else:
            perm = x.new_empty((b, self.m), dtype=torch.long)
            for i in range(b):
                perm[i] = torch.randperm(n)[:self.m]

            x = self.sampling.sample_bdn(x, perm)
            if pts is not None:
                pts = self.sampling.sample_bdn(pts, perm)
            if norm is not None:
                norm = self.sampling.sample_bdn(norm, perm)
            if dist is not None:
                dist = self.sampling.sample_bnn(dist, perm)

        return x, pts, norm, dist

    def __repr__(self):
        return '{name}(m={m})'.format(name=self.__class__.__name__, **self.__dict__)


class MultinomialSampling(torch.autograd.Function):
    """Sample from multinomial random distribution."""

    class Sampler(Enum):
        TOP_K = 1
        RAND_TORCH = 2
        RAND_NUMPY = 3
        RAND_GUMBEL = 4

    # sampling settings
    sampler = Sampler.RAND_GUMBEL

    @staticmethod
    def forward(ctx, prob, x, m):
        # sample m points having the highest probabilities
        if MultinomialSampling.sampler == MultinomialSampling.Sampler.TOP_K:
            sample = torch.topk(prob, k=m, dim=1, largest=True)[1]

        # sample m points randomly according to their probabilities in batch
        elif MultinomialSampling.sampler == MultinomialSampling.Sampler.RAND_TORCH:
            sample = torch.multinomial(prob, num_samples=m, replacement=False)

        # sample m points randomly according to their probabilities in for-loop using NumPy
        elif MultinomialSampling.sampler == MultinomialSampling.Sampler.RAND_NUMPY:
            b, _, n = x.shape
            sample = x.new_empty((b, m), dtype=torch.long)

            for i in range(b):
                prob_i = prob[i].data.cpu().numpy()
                sample_i = np.random.choice(n, size=m, replace=False, p=prob_i)
                sample[i] = torch.from_numpy(sample_i)

        # sample m points pseudo-randomly from re-parametrized Gumbel distribution
        elif MultinomialSampling.sampler == MultinomialSampling.Sampler.RAND_GUMBEL:
            b, _, n = x.shape
            gumbel = torch.distributions.Gumbel(0, 1)
            prob_gumbel = torch.log(prob) + gumbel.rsample((b, n))
            sample = torch.topk(prob_gumbel, k=m, dim=1, largest=True)[1]

        else:
            raise ValueError('Unknown sampling strategy: %s' % MultinomialSampling.sampler)

        sample = torch.sort(sample, dim=1)[0]

        # sample is neither input nor output and therefore must be saved separately
        ctx.save_for_backward(prob, x)
        ctx.sample = sample

        x = Sampling.sample_bdn_loop(x, sample)

        return x

    @staticmethod
    def backward(ctx, grad_x_out):
        prob, x = ctx.saved_tensors
        sample = ctx.sample
        b, _, n = x.shape

        # use tensors that do not require gradient to make indexing possible
        grad_x_out = grad_x_out.data
        prob = prob.data
        x = x.data

        grad_prob = None
        grad_x = None

        if ctx.needs_input_grad[0]:
            grad_prob = torch.zeros_like(prob)

            # sum all gradients corresponding to sampled points and propagate them
            for i in range(b):
                grad_x_i = grad_x_out[i].sum(dim=0)
                log_prob_i = torch.log(prob[i][sample[i]])
                grad_prob[i][sample[i]] = log_prob_i * grad_x_i

            grad_prob.requires_grad_(True)

        if ctx.needs_input_grad[1]:
            grad_x = torch.zeros_like(x)

            # copy gradients to sampled points only
            for i in range(b):
                grad_x[i][:, sample[i]] = grad_x_out[i]

            grad_x.requires_grad_(True)

        # release reference
        ctx.sample = None

        # no gradient for m
        return grad_prob, grad_x, None


class SamplingNet(nn.Module):
    """Network for trainable sampling of points.

    :type m: int
    """

    def __init__(self, m, in_channels, conv_channels, clamp=8):
        super(SamplingNet, self).__init__()

        self.m = m
        self.clamp = clamp

        # the last layer has only one channel which is a score
        c = [in_channels] + list(conv_channels) + [1]

        self.seq = nn.Sequential()
        for i in range(1, len(c)):
            conv = nn.Conv1d(in_channels=c[i - 1], out_channels=c[i], kernel_size=1)
            self.seq.add_module('conv%d' % i, conv)
            if i < len(c) - 1:
                self.seq.add_module('relu%d' % i, nn.ReLU())

        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        b, _, n = x.shape

        # compute sampling score for each point while avoiding gradient propagation on x
        score = self.seq(x.data).view(b, n)

        # clamp scores for a numerical stability in soft-max
        score = torch.clamp(score, -self.clamp, self.clamp)

        # normalize to a valid probability distribution
        prob = self.softmax(score)

        # sample randomly
        x = MultinomialSampling.apply(prob, x, self.m)

        return x, prob

    def __repr__(self):
        s = super(SamplingNet, self).__repr__()
        s = s.splitlines()
        s[0] = '{name}(m={m}'.format(name=self.__class__.__name__, **self.__dict__)
        return '\n'.join(s)


def network_sampling():
    """Test sampling of points whose probabilities are computed by a network."""

    n, m = 128, 32
    num_filters = 4
    num_batches = 2 ** 12

    sampling = SamplingNet(m, in_channels=2, conv_channels=[num_filters])

    w = torch.tensor([[1, 1], [1, -1], [-1, 1], [-1, -1]], dtype=torch.float)
    sampling.seq.conv1.weight = nn.Parameter(w.unsqueeze(2))
    init.constant_(sampling.seq.conv1.bias, 0)
    init.constant_(sampling.seq.conv2.weight, -1)
    init.constant_(sampling.seq.conv2.bias, 0)

    optimizer = torch.optim.SGD(sampling.parameters(), lr=5e-3)

    conv = np.zeros((num_batches, num_filters, 2))
    bias = np.zeros((num_batches, num_filters))
    loss = np.zeros(num_batches)

    for i in range(num_batches):
        # points spread randomly in [-1, 1] x [-1, 1]
        x_rand = np.random.uniform(-1, 1, (1, 2, n)).astype(np.float32)
        x_rand = torch.tensor(x_rand)

        # sample points
        x_sampled, _ = sampling(x_rand)

        # minimize squared L2 norms of sampled points
        norm = x_sampled[0, 0] ** 2 + x_sampled[0, 1] ** 2

        # update weights of sampling network
        optimizer.zero_grad()
        x_sampled.backward(norm)
        optimizer.step()

        params = tuple(sampling.seq[0].parameters())
        conv[i] = params[0].data.squeeze().numpy()
        bias[i] = params[1].data.squeeze().numpy()
        loss[i] = norm.mean().item()

        if (i + 1) % 128 == 0:
            print('%d: %f' % (i + 1, loss[i]))

    # points spread uniformly in [-1, 1] x [-1, 1]
    x = np.linspace(-1, 1, 64, dtype=np.float32)
    x1, x2 = np.meshgrid(x, x)
    x_prob = np.vstack((x1.flat, x2.flat))

    x_prob = torch.tensor(x_prob[np.newaxis])

    # get sampling probabilities for all points
    _, prob = sampling(x_prob)

    # convert to NumPy
    x_rand = x_rand.data.numpy()[0]
    x_sampled = x_sampled.data.numpy()[0]
    x_prob = x_prob.data.numpy()[0]

    pltu.figure('Learned distribution, points and weights')
    plt.plot(x_rand[0], x_rand[1], 'c+')
    plt.plot(x_sampled[0], x_sampled[1], 'mx')
    plt.scatter(x_prob[0], x_prob[1], c=prob.data.numpy()[0])
    plt.colorbar()
    plt.plot(conv[:, :, 0], conv[:, :, 1])
    pltu.show(block=False, equal_aspect=True)

    pltu.figure('Biases and loss')
    plt.plot(bias)
    plt.plot(loss)
    pltu.show()


def normal_sampling():
    """Test sampling from normal distribution."""

    num_pts = 100
    num_epochs = 10000

    linear = nn.Linear(in_features=num_pts, out_features=1, bias=False)
    sigma = torch.tensor(1, dtype=torch.float, requires_grad=True)

    optimizer = torch.optim.SGD(list(linear.parameters()) + [sigma], lr=1e-3, momentum=0.9)

    dists = list()
    sigmas = list()
    devs = list()

    for epoch in range(num_epochs):
        a = np.random.uniform(0, 1)
        b = np.random.uniform(0, 1)
        x = a * torch.rand(num_pts) + b

        f = linear(x)

        # d = (x.mean() - f) ** 2

        normal = torch.distributions.Normal(loc=f, scale=sigma)
        z = normal.rsample((1,))
        d = (x.mean() - z) ** 2

        optimizer.zero_grad()
        d.backward()
        optimizer.step()

        dists.append(d.item())
        sigmas.append(sigma.item())

        w = linear.weight.data.numpy().squeeze()
        devs.append(np.std(w))

        print(epoch, d.item(), sigma.item(), np.sum(w), np.std(w))

    pltu.figure('Training progress')
    plt.plot(dists)
    plt.plot(sigmas)
    plt.plot(devs)
    pltu.show()

    pltu.figure('Final weights')
    plt.plot(w)
    pltu.show()


def gumbel_sampling():
    """Test sampling from categorical distribution reparametrized with Gumbel distribution."""

    num_samples = 1000
    num_categories = 4
    probs = torch.tensor([0.1, 0.2, 0.3, 0.4], requires_grad=True)

    categorical = torch.distributions.Categorical(probs)
    x_categorical = categorical.sample((num_samples,))

    loc = torch.tensor(0.0, requires_grad=True)
    scale = torch.tensor(1.0, requires_grad=True)
    gumbel = torch.distributions.Gumbel(loc, scale)

    x_gumbel = gumbel.rsample((num_samples, num_categories)) + torch.log(probs)
    p_gumbel, x_gumbel = torch.max(x_gumbel, dim=1)

    print('Categorical:', np.bincount(x_categorical.data.numpy()))
    print('Gumbel:', np.bincount(x_gumbel.data.data.numpy()))

    p_gumbel.backward(gradient=torch.ones(num_samples))

    print('Probability:', probs.grad)
    print('Location:', loc.grad)
    print('Scale:', scale.grad)


def main():
    b, d, n, m = 4, 3, 16, 8

    utils.random_seed(0)

    network_sampling()
    # normal_sampling()
    # gumbel_sampling()

    x = torch.rand(b, d, n)

    dist = spatial.Distance()(x.data)

    sampling = RandomSampling(m, batch_wise=True)
    x_sampled, pts_sampled, _, dist_sampled = sampling(x, x.data, None, dist)
    print('RandomSampling', x_sampled.size(), pts_sampled.size(), dist_sampled.size())

    sampling = SamplingNet(m, in_channels=d, conv_channels=[4])
    x_sampled, prob = sampling(x)
    print('SamplingNet', x_sampled.size(), prob.size())

    # test back propagation
    norm = x_sampled.norm()
    optimizer = torch.optim.SGD(sampling.parameters(), lr=0.01)
    optimizer.zero_grad()
    norm.backward()
    optimizer.step()


if __name__ == '__main__':
    main()
