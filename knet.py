from __future__ import division, print_function
import numpy as np
import torch
import torch.autograd
import torch.cuda
import torch.nn as nn
import torch.nn.functional
import torch.nn.init as init

import kconv
import kpars
import math
import sampling
import spatial
import tensors
import utils


class ConvLayer(nn.Module):
    """Convolution (independent or over k nearest neighbors) layer with optional
    batch normalization and activation.

    :type m: int
    :type k_search: int
    :type k_conv: int
    :type activation: torch.nn.Module
    """

    def __init__(self, m, k_search, k_conv, activation, append_points, append_normals):
        super(ConvLayer, self).__init__()

        self.k_conv = k_conv

        self.sampling = None
        if m > 0:
            self.sampling = sampling.RandomSampling(m)

        self.search = None
        if k_search > 0:
            self.search = spatial.KnnSearch(k_search)

        self.append_points = append_points
        self.append_normals = append_normals

        self.activation = activation

    def forward(self, x, pts, norm, dist, knn):
        # sub-sample points and distances
        if self.sampling is not None:
            x, pts, norm, dist = self.sampling(x, pts, norm, dist)

        # search for k-NN of each point
        if self.search is not None:
            knn = self.search(dist)[0]

        # append points and normals to features
        feat = [x]
        if self.append_points:
            feat.append(pts)
        if self.append_normals:
            feat.append(norm)
        if len(feat) > 1:
            x = torch.cat(feat, dim=1)

        if self.k_conv <= 1:
            x = self.conv(x)
        else:
            x = self.conv(x, knn)

        if self.batch_norm is not None:
            x = self.batch_norm(x)

        if self.activation is not None:
            x = self.activation(x)

        return x, pts, norm, dist, knn

    @staticmethod
    def append_in_channels(in_channels, append_points, append_normals):
        if append_points:
            in_channels += 3
        if append_normals:
            in_channels += 3
        return in_channels

    def extra_repr(self):
        return 'points={append_points}, normals={append_normals}'.format(**self.__dict__)


class FullConvLayer(ConvLayer):
    """Convolution (independent or over k nearest neighbors) layer with optional
    batch normalization and activation.

    :type in_channels: int
    :type out_channels: int
    :type batch_norm: torch.nn.Module
    """

    def __init__(self, m, k_search, k_conv, in_channels, out_channels, center,
                 batch_norm, activation, append_points, append_normals):

        super(FullConvLayer, self).__init__(m, k_search, k_conv, activation,
                                            append_points, append_normals)

        self.batch_norm = None
        if batch_norm:
            self.batch_norm = nn.BatchNorm1d(out_channels)

        in_channels = self.append_in_channels(in_channels, append_points, append_normals)

        if k_conv <= 1:
            self.conv = nn.Conv1d(in_channels, out_channels, kernel_size=1)
        else:
            self.conv = kconv.KnnConvFull(in_channels, out_channels, k_conv, center)


class PairConvLayer(ConvLayer):
    """Convolution (independent or over k nearest neighbors) layer with optional
    batch normalization and activation.

    :type in_channels: int
    :type out_channels: int
    :type pool_type: str
    :type k_pool: int
    :type batch_norm: torch.nn.Module
    """

    def __init__(self, m, k_search, k_conv, in_channels, out_channels, pool_type, k_pool,
                 batch_norm, activation, append_points, append_normals):

        super(PairConvLayer, self).__init__(m, k_search, k_conv, activation,
                                            append_points, append_normals)

        in_channels = self.append_in_channels(in_channels, append_points, append_normals)

        if k_conv <= 0:
            self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)
        else:
            self.conv = kconv.KnnConvPair(in_channels, out_channels, k_conv)

        self.batch_norm = None
        if batch_norm:
            self.batch_norm = nn.BatchNorm2d(out_channels)

        self.pool = None
        if pool_type == 'avg':
            self.pool = nn.AvgPool2d(kernel_size=(k_pool, 1))
        elif pool_type == 'max':
            self.pool = nn.MaxPool2d(kernel_size=(k_pool, 1))
        elif pool_type == 'sum':
            self.pool = kconv.SumPool(weight_size=(1, out_channels, k_pool, 1), reduce_dim=2)
        elif pool_type == 'spatial':
            self.pool = kconv.SpatialPool(k_pool, out_channels)

    def forward(self, x, pts, norm, dist, knn):
        x, pts, norm, dist, knn = super(PairConvLayer, self).forward(x, pts, norm, dist, knn)

        if isinstance(self.pool, kconv.SpatialPool):
            x = self.pool(x, pts, norm, dist, knn).squeeze(2)
        elif self.pool is not None:
            x = self.pool(x).squeeze(2)

        return x, pts, norm, dist, knn


class ViewLayer(nn.Module):
    """Layer responsible for reshaping the input.

    :type shape: tuple[int]
    """

    def __init__(self, *args):
        super(ViewLayer, self).__init__()
        self.shape = args

    def forward(self, x):
        return x.view(self.shape)

    def __repr__(self):
        return '{name}(shape={shape})'.format(name=self.__class__.__name__, **self.__dict__)


class LinearLayer(nn.Module):
    """Fully connected layer with optional batch normalization and activation.

    :type in_features: int
    :type out_features: int
    :type batch_norm: bool
    :type activation: torch.nn.Module
    :type drop: float
    """

    def __init__(self, in_features, out_features, drop, batch_norm, activation):
        super(LinearLayer, self).__init__()

        self.f = out_features

        self.drop = None
        if drop > 0:
            self.drop = nn.Dropout(p=drop)

        self.linear = nn.Linear(in_features, out_features)

        self.batch_norm = None
        if batch_norm:
            self.batch_norm = nn.BatchNorm1d(out_features)

        self.activation = activation

    def forward(self, x):
        if self.drop is not None:
            x = self.drop(x)

        x = self.linear(x)

        if self.batch_norm is not None:
            x = self.batch_norm(x)

        if self.activation is not None:
            x = self.activation(x)

        return x


class PoolingAggregator(nn.Module):
    """Aggregator of local features based on pooling."""

    def __init__(self, pool_size, out_size):
        super(PoolingAggregator, self).__init__()

        self.maxpool = nn.MaxPool1d(pool_size)
        self.view = ViewLayer(-1, out_size)

    def forward(self, x, _):
        x = self.maxpool(x)
        x = self.view(x)
        return x


class RecurrentAggregator(nn.Module):
    """Aggregator of local features based on recurrent network."""

    def __init__(self, in_out_size, recurrent_unit=nn.GRU, **kwargs):
        super(RecurrentAggregator, self).__init__()

        self.ind = tensors.BatchIndex()
        self.recurrent = recurrent_unit(input_size=in_out_size, hidden_size=in_out_size, **kwargs)

    def forward(self, x, pts):
        b, d, n = x.shape

        # find indices of points sorted according to their norm in ascending order
        norm = pts.norm(dim=1)
        ind = norm.sort(dim=1)[1].view(-1)

        # reorganize local features base on norm of points
        x = x.transpose(1, 2)
        x = x[self.ind.get(x, n), ind].view(b, n, d)

        # (b, n, d) -> (n, b, d), which is convenient for recurrent networks
        x = x.permute(1, 0, 2)

        x = self.recurrent(x)[0]

        # keep only the last of the outputs for n points
        x = x[-1]

        return x


class KnnNet(nn.Module):
    """Network using convolution layers and fully connected layers."""

    def __init__(self, pars):
        super(KnnNet, self).__init__()

        self.pars = pars

        # spatial distance to search for k-NN
        self.distance = spatial.Distance()

        # number of k-NN to use in search (to avoid repeated searches)
        k_search = self.k_search(pars.m, pars.k_conv)

        # number of k-NN to use in pairwise pooling
        k_pool = None
        if pars.conv_type == 'pair':
            k_pool = self.k_pool(pars.k_conv, pars.pool_type)

        # convolutional layers for features extraction
        self.conv = nn.ModuleList()
        for i, p in enumerate(pars.conv):
            in_size = pars.input_dim if i == 0 else pars.conv[i - 1].size
            activation = tensors.activation_module(p.activation)
            if pars.conv_type == 'full':
                layer = FullConvLayer(p.m, k_search[i], p.k, in_size, p.size, p.center,
                                      p.batch_norm, activation, p.points, p.normals)
            elif pars.conv_type == 'pair':
                layer = PairConvLayer(p.m, k_search[i], p.k, in_size, p.size, p.pool_type,
                                      k_pool[i], p.batch_norm, activation, p.points, p.normals)
            else:
                raise ValueError('Unknown convolution type (must be pair or full).')
            self.conv.append(layer)

        self.aggregator = None
        if pars.aggregator == 'pool':
            self.aggregator = PoolingAggregator(self.pool_size(pars.num_pts, pars.m),
                                                pars.conv[-1].size)
        else:
            recurrent_unit = eval('torch.nn.' + pars.aggregator)
            self.aggregator = RecurrentAggregator(pars.conv[-1].size, recurrent_unit)

        # linear layers for classification
        self.lin = nn.ModuleList()
        for i, p in enumerate(pars.lin):
            in_size = pars.conv[-1].size if i == 0 else pars.lin[i - 1].size
            self.lin.append(LinearLayer(in_size, p.size, p.drop, p.batch_norm,
                                        tensors.activation_module(p.activation)))

        self.init_params()

    def forward(self, x):
        # extract 3D points
        pts = x.data[:, :3]

        # extract 3D normals if available
        norm = None if (x.shape[1] == 3) else x.data[:, 3:6]

        # pre-compute pairwise distances
        dist = self.distance(pts)

        # compute point-wise features
        knn = None
        for conv in self.conv:
            x, pts, norm, dist, knn = conv(x, pts, norm, dist, knn)

        # aggregate point-wise features to global feature vector
        feat = x = self.aggregator(x, pts)

        # classify using fully connected layers
        for lin in self.lin:
            x = lin(x)

        return x, feat

    @staticmethod
    def k_search(m, k_conv):
        """For each layer find the number of k-NN to search for."""

        k_search = len(k_conv) * [0]

        i = 0
        for j in range(len(k_conv)):
            if m[j] > 0:
                i = j
            k_search[i] = max(k_search[i], k_conv[j] + 1)

        return k_search

    @staticmethod
    def k_pool(k_conv, pool):
        """For each layer find the number of k-NN to pool over."""

        k_pool = len(k_conv) * [0]

        k = 0
        for i in range(len(pool)):
            if k_conv[i] > 0:
                k = k_conv[i]
            if pool[i] is not None and len(pool[i]) > 0:
                k_pool[i] = k

        return k_pool

    @staticmethod
    def pool_size(num_pts, m):
        """Find size of max-pooling kernel."""
        m = np.concatenate(([num_pts], m))
        return int(np.min(m[m > 0]))

    def summary(self):
        return str(self.pars)

    def init_params(self):
        for layer in self.conv:
            conv = layer.conv
            if type(conv) in (kconv.KnnConvFull, kconv.KnnConvPair):
                conv = conv.conv

            init.kaiming_uniform_(conv.weight,
                                  nonlinearity=tensors.activation_name(layer.activation))
            init.constant_(conv.bias, 0)

            # initialize pooling weights by Kaiming He: sqrt(6 / n_in)
            if hasattr(layer, 'pool'):
                pool = layer.pool

                if isinstance(pool, kconv.SumPool):
                    reduce_size = np.prod(np.array(pool.weight_size)[np.array(pool.reduce_dim)])
                    a = math.sqrt(6 / reduce_size)
                    init.uniform_(pool.weight, -a, a)
                    init.constant_(pool.bias, 0)

                elif isinstance(pool, kconv.SpatialPool):
                    a = math.sqrt(6 / pool.k)
                    init.uniform_(pool.weight, -a, a)

        if isinstance(self.aggregator, RecurrentAggregator):
            recurrent = self.aggregator.recurrent
            size = recurrent.hidden_size
            mode = recurrent.mode

            for name, param in recurrent.named_parameters():
                # use Kaiming with proper gain for weights
                if name.startswith('weight'):
                    if mode.startswith('RNN'):
                        init.kaiming_uniform_(param, nonlinearity=mode.split('_')[1].lower())
                    elif mode in ('GRU', 'LSTM'):
                        init.kaiming_uniform_(param[:2*size], nonlinearity='sigmoid')
                        init.kaiming_uniform_(param[2*size:3*size], nonlinearity='tanh')
                        if mode == 'LSTM':
                            init.kaiming_uniform_(param[3*size:], nonlinearity='sigmoid')

                elif name.startswith('bias'):
                    init.constant_(param, 0)

        for layer in self.lin:
            init.kaiming_uniform_(layer.linear.weight,
                                  nonlinearity=tensors.activation_name(layer.activation))
            init.constant_(layer.linear.bias, 0)


def main():
    b, d, n, k = 4, 6, 1024, 8
    device = 'cpu'

    utils.random_seed(0)

    x = torch.rand(b, d, n, device=device)
    y = torch.randint(0, k, (b, ), dtype=torch.long, device=device)

    pars = kpars.KnnNetParams(n, 10, 'full', d)
    net = KnnNet(pars).to(device)

    print(net)
    print(utils.count_params(net))
    print(net.summary())

    f, feat = net(x)

    loss = torch.nn.functional.nll_loss(f, y)
    loss.backward()


if __name__ == '__main__':
    main()
