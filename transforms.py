from __future__ import print_function
import numpy as np

import plot_utils as pltu


FEATURE_PATH = 'data/certh_feat.npz'
SHOW_IND = 100
SHOW_CLASS = -1
SCALE = 0.1
VISUALIZE = True


def plot_transforms(tx, ty, tz, title, block=False):
    pltu.figure(title)
    pltu.quiver(0, SCALE * np.eye(3), colors='k')
    pltu.quiver(0, SCALE * tx, colors='r')
    pltu.quiver(0, SCALE * ty, colors='g')
    pltu.quiver(0, SCALE * tz, colors='b')
    pltu.axis_equal_3d()
    pltu.axis_label_3d('x', 'y', 'z')
    pltu.show(block)


def reprojection_error(trans, x1, x2):
    diff = np.matmul(x1, trans) - x2
    diff = diff * diff
    dist = np.sqrt(np.sum(diff, axis=1))
    error = np.mean(dist)
    return error


data = np.load(FEATURE_PATH)
x = data['x']
y = data['y']
t = data['t']

# select only certain class
if SHOW_CLASS >= 0:
    x = x[y == SHOW_CLASS]
    t = t[y == SHOW_CLASS]

# transform each point-cloud with the corresponding STN3D matrix
xt = np.matmul(x, t)
# xt = np.matmul(x, t.transpose([0, 2, 1]))

xi = x[SHOW_IND]
xti = xt[SHOW_IND]

if VISUALIZE:
    pltu.figure('Points')
    pltu.scatter(xi[:, 0], xi[:, 1], xi[:, 2], c='b')
    # pltu.scatter(xti[:, 0], xti[:, 1], xti[:, 2], c='r')
    pltu.axis_equal_3d()
    pltu.axis_label_3d('x', 'y', 'z')
    pltu.show(block=False)

t /= np.linalg.norm(t, axis=(1, 2), keepdims=True)

if VISUALIZE:
    plot_transforms(t[:, 0], t[:, 1], t[:, 2], 'Transforms')

# vectors of all original and transformed points
xv = np.reshape(x, (-1, 3))
xtv = np.reshape(xt, (-1, 3))

a = np.zeros((3 * xv.shape[0], 9))
for i in range(xv.shape[0]):
    for j in range(3):
        a[3*i+j, j*3:(j+1)*3] = xv[i]

b = xtv.flatten()

t_lstsq = np.linalg.lstsq(a, b)[0]
t_lstsq = np.reshape(t_lstsq, (3, 3), order='F')
print('Least squares: %f\n' % reprojection_error(t_lstsq, xv, xtv), t_lstsq)

if VISUALIZE:
    t_lstsq /= np.linalg.norm(t_lstsq)
    plot_transforms(t_lstsq[0], t_lstsq[1], t_lstsq[2], 'Least squares transform')

t_mean = np.mean(t, axis=0)
print('Mean: %f\n' % reprojection_error(t_mean, xv, xtv), t_mean)

if VISUALIZE:
    t_mean /= np.linalg.norm(t_mean)
    plot_transforms(t_mean[0], t_mean[1], t_mean[2], 'Mean transform', True)
