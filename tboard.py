import tensorboardX
import termcolor
import torch
import torch.nn


class ModelWrapper(torch.nn.Module):
    """Wrapper for model to be written as graph."""

    def __init__(self, model):
        super(ModelWrapper, self).__init__()
        self.model = model

    def forward(self, x):
        f = self.model(x)

        # recursively extract the first out
        while isinstance(f, tuple):
            f = f[0]

        return f


class ModelWriter:
    """Writer to TensorBoard.

    :type writer: tensorboardX.SummaryWriter
    :type model: torch.nn.Module
    :type epoch: int
    :type training: bool
    """

    def __init__(self, output_dir, model):
        self.writer = tensorboardX.SummaryWriter(log_dir=output_dir)
        self.model = model
        self.epoch = 0
        self.training = model.training

    def __del__(self):
        self.writer.close()

    def next_epoch(self, epoch, training):
        """Proceed to the next training/testing epoch."""
        self.epoch = epoch
        self.training = training

    def flush(self):
        """Flush writer to opened file."""
        self.writer.file_writer.flush()

    def graph(self, input_shape):
        """Write model graph to TensorBoard.

        :type input_shape: tuple[int]
        """

        # create dummy input
        device = next(self.model.parameters()).device
        x = torch.empty(input_shape, device=device, requires_grad=True)

        # wrap the model to deal with possibly multiple outputs
        model = ModelWrapper(self.model)

        try:
            self.writer.add_graph(model, x)
            self.flush()
        except TypeError:
            termcolor.cprint('Model graph not written to TensorBoard.', color='red')

    def scalar(self, name, value, prefix=True):
        """Write scalar value to TensorBoard.

        :type name: str
        :type value: int | float
        :type prefix: bool
        """
        if prefix:
            name = '%s/%s' % ('train' if self.training else 'test', name)
        self.writer.add_scalar(name, value, self.epoch)
        self.flush()

    def histograms(self):
        """Write histograms of model weights to TensorBoard."""
        for name, param in self.model.named_parameters():
            try:
                self.writer.add_histogram(name.replace('.', '/'),
                                          param.data.cpu().numpy(),
                                          self.epoch, bins='sturges')
            except MemoryError:
                termcolor.cprint('Epoch %d: Unable to compute histogram.' % self.epoch,
                                 color='red')

        self.flush()

    def summary(self):
        """Write model summary string to TensorBoard."""
        summary = str(self.model)
        summary = summary.replace('\n', '<br/>').replace(' ', '&nbsp;')
        self.writer.add_text('model', summary)
        self.flush()


def load_accuracy_summary(file_path):
    """Load TensorBoard events related to train/test accuracy and model summary.

    :type file_path: str
    :rtype: (List[float], List[float], List[float], str)
    """

    import tensorflow as tf

    acc_train = []
    acc_test = []
    lr = []
    summary = ''

    for event in tf.train.summary_iterator(file_path):
        if len(event.summary.value) == 1:
            value = event.summary.value[0]
            if value.tag == 'train/accuracy':
                acc_train.append(value.simple_value)
            elif value.tag == 'test/accuracy':
                acc_test.append(value.simple_value)
            elif value.tag == 'learning_rate':
                lr.append(value.simple_value)
            elif value.tag == 'model/text_summary' or value.node_name == 'model':
                summary = value.tensor.string_val[0].decode('utf-8')
                summary = summary.replace('&nbsp;', ' ').replace('<br/>', '\n')

    return acc_train, acc_test, lr, summary
