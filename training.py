from __future__ import print_function
import logging
import os
import sys
import torch.autograd
import torch.nn.functional
import torch.optim
import torch.utils.data

import bar as progress
import datasets
import evaluation
import tboard
import utils


class Trainer:
    """Trainer of neural networks."""

    def __init__(self, params, model, optimizer, lr_schedule, num_epochs, batch_size,
                 path_train, path_test, num_pts, normalize, normals, augmentation,
                 device, seed=0, test_step=1, save_step=10, show_bar=True):

        self.model = model
        self.optimizer = optimizer
        self.lr_schedule = lr_schedule
        self.num_epochs = num_epochs
        self.augmentation = augmentation
        self.device = torch.device(device)
        self.test_step = test_step
        self.show_bar = show_bar

        utils.random_seed(seed)

        data_train = datasets.NumPyDataset(path_train, normalize=normalize, normals=normals)
        data_test = datasets.NumPyDataset(path_test, num_pts, normalize=normalize, normals=normals)
        self.loader_train = self.load_data(data_train, batch_size, device)
        self.loader_test = self.load_data(data_test, batch_size, device)
        self.num_classes = len(data_train.classes)

        self.model.to(device)

        output_dir = utils.build_output_dir('out', model)

        self.log = self.create_log(output_dir)
        self.log.debug('Model:\n%s' % model)
        self.log.debug('Parameters:\n%s' % utils.count_params(model))
        self.log.debug('Augmentation: %s' % augmentation)
        self.log.debug('Optimizer: %s(%s)' % (optimizer.__class__.__name__, optimizer.defaults))
        self.log.debug('LR: %s(%s)' % (lr_schedule.__class__.__name__, utils.lr_schedule_params(lr_schedule)))
        self.log.debug('Output: %s' % output_dir)
        self.log.debug('Seed: %s' % seed)
        self.log.debug('Data:\n%s' % utils.data_summary(data_train.y, data_test.y, data_train.classes))
        if self.device.type == 'cuda':
            self.log.debug(utils.gpu_memory_usage())

        self.writer = tboard.ModelWriter(output_dir, model)
        self.writer.summary()

        self.saver = utils.ModelSaver(output_dir, model, optimizer, lr_schedule,
                                      num_epochs, save_step)

        utils.save_params(output_dir, params)
        utils.backup_sources(os.path.join(output_dir, 'backup'))

    @staticmethod
    def load_data(data, batch_size, device):
        """Load and prepare the input data.

        :type data: datasets.NumPyDataset
        :type batch_size: int
        :type device: torch.Device
        :rtype: torch.utils.data.DataLoader
        """

        x = data.x.transpose((0, 2, 1))
        x = torch.tensor(x)
        x = x.to(device)

        y = torch.tensor(data.y)
        y = y.to(device)

        data = torch.utils.data.TensorDataset(x, y)
        loader = torch.utils.data.DataLoader(data, batch_size, shuffle=True)

        return loader

    @staticmethod
    def create_log(output_dir):
        """Create log and add stdout and file handler to it.

        :type output_dir: str
        :rtype: logging.Logger
        """

        log = logging.getLogger(output_dir)
        log.setLevel(logging.DEBUG)
        log.propagate = False

        # file handler
        file_log = logging.FileHandler(os.path.join(output_dir, 'log.txt'))
        file_log.setLevel(logging.DEBUG)
        log.addHandler(file_log)

        # stdout handler
        stdout_log = logging.StreamHandler(sys.stdout)
        stdout_log.setFormatter(logging.Formatter('{} - %(message)s'.format(os.getpid())))
        stdout_log.setLevel(logging.INFO)
        log.addHandler(stdout_log)

        return log

    def update_lr(self, epoch, is_training, loss):
        """Update learning rate according to a schedule.

        :type epoch: int
        :type is_training: bool
        :type loss: float
        """

        # update learning rate if the validation loss plateaus
        if isinstance(self.lr_schedule, torch.optim.lr_scheduler.ReduceLROnPlateau):
            if not is_training:
                self.lr_schedule.step(loss, epoch)

        # update learning rate according to predefined schedule
        elif self.lr_schedule is not None:
            if is_training:
                self.lr_schedule.step(epoch)

    def log_stats(self, epoch, is_training, evaluator, bar):
        """Write evaluation to the log.

        :type epoch: int
        :type is_training: bool
        :type evaluator: evaluation.Evaluator
        :type bar: progress.TrainingBar
        """

        self.log.info('Epoch %d (%s): accuracy=%.3f class=%.3f time=%s' % (
            epoch, 'train' if is_training else 'test',
            evaluator.mean_accuracy, evaluator.mean_class_accuracy, bar.elapsed_td))

        if not is_training:
            self.log.debug('Class accuracy: %s', evaluator.class_accuracy)

        if self.device.type == 'cuda':
            self.log.debug(utils.gpu_memory_usage())

    def write_tensorboard(self, epoch, is_training, evaluator):
        """Write evaluation of the epoch to TensorBoard.

        :type epoch: int
        :type is_training: bool
        :type evaluator: evaluation.Evaluator
        """

        self.writer.next_epoch(epoch, is_training)

        self.writer.scalar('loss', evaluator.mean_loss)
        self.writer.scalar('accuracy', evaluator.mean_accuracy)
        self.writer.scalar('class_accuracy', evaluator.mean_class_accuracy)

        if is_training:
            self.writer.scalar('learning_rate', self.optimizer.param_groups[0]['lr'], prefix=False)
            self.writer.histograms()

    def process_batch(self, epoch, is_training):
        """Process data in batches either in training or testing mode.

        :type epoch: int
        :type is_training: bool
        :rtype: evaluation.Evaluator
        """

        loader = self.loader_train if is_training else self.loader_test

        num_batches = len(loader)
        evaluator = evaluation.Evaluator(self.num_classes)

        title = 'Train' if is_training else 'Test'
        bar = progress.TrainingBar(epoch, self.num_epochs, num_batches, title)

        # run model in training or evaluation mode
        self.model.train(is_training)

        for x, y in loader:
            if is_training and self.augmentation is not None:
                x = self.augmentation.apply(x)

            # forward step
            f = self.model(x)[0]
            loss = torch.nn.functional.nll_loss(f, y)

            # backward step
            if is_training:
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()

            # convert predicted class probabilities to class labels
            f = f.max(dim=1)[1]
            loss = loss.item()

            accuracy = evaluator.batch(f.data.cpu().numpy(), y.data.cpu().numpy(), loss)

            if self.show_bar:
                bar.next(loss, accuracy)

        if self.show_bar:
            bar.finish()

        self.update_lr(epoch, is_training, evaluator.mean_loss)

        self.log_stats(epoch, is_training, evaluator, bar)
        self.write_tensorboard(epoch, is_training, evaluator)
        self.saver.save(epoch)

    def train(self):
        """Train the model."""

        for epoch in range(1, self.num_epochs + 1):
            # training
            self.process_batch(epoch, is_training=True)

            # testing
            if (self.test_step > 0 and epoch % self.test_step == 0) or epoch == self.num_epochs:
                with torch.autograd.no_grad():
                    self.process_batch(epoch, is_training=False)

    def check(self):
        """Check forward pass on the model."""

        print(self.model)
        print(utils.count_params(self.model))

        # data
        x, y = next(iter(self.loader_train))
        if self.augmentation is not None:
            x = self.augmentation.apply(x)

        # forward step
        f = self.model(x)[0]
        torch.nn.functional.nll_loss(f, y)
