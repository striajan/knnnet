from __future__ import division, print_function
import numpy as np
import torch.autograd

import bar as progress
import datasets
import knet
import kpars
import pointnet
import transnet
import utils


PARAMS_PATH = 'data/params.pkl'
MODEL_PATH = 'data/model.pth'
PCD_PATH = 'data/doumanoglou-pcd.npz'
FEAT_PATH = 'data/doumanoglou.npz'
DEVICE = 'cpu'
BATCH_SIZE = 32
NUM_POINTS = 1024
NUM_CLASSES = 55
CONV_TYPE = 'full'

data = datasets.NumPyDataset(PCD_PATH, NUM_POINTS)
num_data = len(data.x)
num_classes = len(data.classes)

# load complete classification model
# params = kpars.KnnNetParams(NUM_POINTS, num_classes, CONV_TYPE)
params = utils.load_params(PARAMS_PATH)
model = knet.KnnNet(params)
# model = pointnet.PointNetCls(NUM_POINTS, NUM_CLASSES)
state = torch.load(MODEL_PATH, map_location=torch.device(DEVICE))
model.load_state_dict(state)

# run in evaluation mode (no batch-norm and dropout) without gradients
model.eval()
for param in model.parameters():
    param.requires_grad_(False)

# replace transformation network with fixed transformation matrix
# model.trans = transnet.TransMat()

model = model.to(DEVICE)

num_batches = int(np.ceil(len(data) / BATCH_SIZE))
bar = progress.TimeBar(num_batches, 'Features')

feat = list()

for batch in range(num_batches):
    # select batch of data
    batch_ind = slice(batch * BATCH_SIZE, min(num_data, (batch + 1) * BATCH_SIZE))
    x = torch.from_numpy(data.x[batch_ind]).transpose(2, 1)
    x = x.to(DEVICE)

    # compute features
    f = model(x)[1]
    feat.append(f.data.cpu().numpy())

    bar.next()

bar.finish()

np.savez(FEAT_PATH, x=data.x, y=data.y, f=np.concatenate(feat),
         classes=data.classes, names=data.data['names'])
