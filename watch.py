from __future__ import print_function
import timeit
import torch.cuda


class Watch:
    """Class for measuring execution time of code.

    :type name: str
    :type verbose: bool
    :type start: float
    :type end: float
    :type interval: float
    """

    def __init__(self, name='Duration', verbose=True):
        self.name = name
        self.verbose = verbose

    def __enter__(self):
        self.start = timeit.default_timer()
        return self

    def __exit__(self, *args):
        self.end = timeit.default_timer()
        self.interval = self.end - self.start
        if self.verbose:
            print(u'{0}: {1} s'.format(self.name, self.interval))


class CudaWatch(Watch):
    """Class for measuring execution time of CUDA code."""

    def __init__(self, *args, **kwargs):
        super(CudaWatch, self).__init__(*args, **kwargs)

    def __enter__(self):
        torch.cuda.synchronize()
        return super(CudaWatch, self).__enter__()

    def __exit__(self, *args):
        torch.cuda.synchronize()
        super(CudaWatch, self).__exit__(*args)
