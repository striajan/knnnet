from __future__ import print_function
import h5py
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.io
import scipy.ndimage

import plot_utils as pltu
import tboard


def find_key(dictionary, keys):
    for k in keys:
        if k in dictionary:
            return dictionary[k]

    raise KeyError('Key not found')


def show_certh_mat(mat_path, items, min_depth=800, max_depth=np.inf):
    """Show Matlab matrix containing depths."""

    # new Matlab format utilizes HDF5
    if h5py.is_hdf5(mat_path):
        with h5py.File(mat_path) as data:
            x = np.squeeze(find_key(data, ('X', 'Xtr', 'X1')))
            y = np.squeeze(find_key(data, ('y', 'ytr', 'y1')))

    # old proprietary Matlab format
    else:
        data = scipy.io.loadmat(mat_path)
        x = data['X'].transpose(2, 0, 1)
        y = data['Y'].squeeze()

    print('Loaded %d data items' % x.shape[0])

    depths = np.asarray(x[items], dtype=np.float)
    depths[(depths < min_depth) | (depths > max_depth)] = np.nan
    if depths.shape[1] < depths.shape[2]:
        depths = depths.transpose(0, 2, 1)

    pltu.imarray(depths, 'Depths', np.asarray(y[items], dtype=int),
                 show_axes=False, show_colorbar=False)


def show_certh_png(png_path, min_depth=500, max_depth=1500):
    """Show 16-bit PNG image containing depths."""

    depth = scipy.ndimage.imread(png_path, mode='F')
    if depth.shape[0] < depth.shape[1]:
        depth = depth.transpose()
    depth[(depth < min_depth) | (depth > max_depth)] = np.nan

    pltu.imshow(depth, cmap='plasma')


def show_corona(npy_path, items):
    """Show NPY file containing depths."""

    x = np.load(npy_path)
    print('Loaded %d data items' % x.shape[0])
    pltu.imarray(x[items], os.path.basename(npy_path),
                 show_axes=False, show_colorbar=False)


def show_certh_dps(dps_path):
    """Show DPS file containing a depth map."""

    with open(dps_path, mode='r') as f:
        data = np.fromstring(f.read(), dtype=np.uint16)

    w = data[0]
    h = data[1]
    depth = np.reshape(data[2:], (h, w)).astype(float)
    depth[depth == 0] = np.nan

    pltu.imshow(depth, cmap='plasma')


def show_poses(poses_path, show_rotation=False):
    """Show text file containing camera poses."""

    poses = np.loadtxt(poses_path)
    trans = poses[:, :3]
    rot = poses[:, 3:]
    rot /= np.linalg.norm(rot, axis=1)[:, np.newaxis]

    pltu.figure('Camera poses: %s' % os.path.basename(poses_path))
    pltu.scatter(*trans.transpose())
    if show_rotation:
        pltu.quiver(trans, rot)
    pltu.axis_equal_3d()
    pltu.show()


def show_modelnet(x, y, i=-1):
    """Show 3D point cloud from ModelNet40 dataset."""

    class_names = [
        'airplane', 'bathtub', 'bed', 'bench', 'bookshelf', 'bottle', 'bowl', 'car',
        'chair', 'cone', 'cup', 'curtain', 'desk', 'door', 'dresser', 'flower_pot',
        'glass_box', 'guitar', 'keyboard', 'lamp', 'laptop', 'mantel', 'monitor', 'night_stand',
        'person', 'piano', 'plant', 'radio', 'range_hood', 'sink', 'sofa', 'stairs',
        'stool', 'table', 'tent', 'toilet', 'tv_stand', 'vase', 'wardrobe', 'xbox']

    x = x.cpu().numpy()
    y = y.cpu().numpy()

    if x.ndim == 3:
        if 0 <= i < x.shape[0]:
            x = x[i:i+1]
            y = y[i:i+1]
            b = 1
        else:
            b = x.shape[0]
    elif x.ndim == 2:
        x = x[np.newaxis]
        y = y[np.newaxis]
        b = 1

    fig = pltu.figure()
    rows, cols = pltu.layout(b)

    for i in range(b):
        ax = fig.add_subplot(rows, cols, i + 1, projection='3d')
        ax.scatter(x[i][0], x[i][1], x[i][2])
        ax.set_title('%d - %s' % (y[i], class_names[y[i]]))
        pltu.axis_equal_3d(ax)
        if b > 1:
            ax.set_axis_off()
        else:
            pltu.axis_label_3d('x', 'y', 'z', ax)

    pltu.show()


def plot_accuracies(dir_path):
    """Plot accuracies stored as TensorBoard events."""

    for model_name in sorted(os.listdir(dir_path)):
        for file_name in sorted(os.listdir(os.path.join(dir_path, model_name))):
            if file_name.startswith('events.out.tfevents'):
                file_path = os.path.join(dir_path, model_name, file_name)
                acc_train, acc_test, lr, summary = tboard.load_accuracy_summary(file_path)

                print(summary)
                print('Train: %.3f %.3f' % (acc_train[-1], np.max(acc_train)))
                print('Test: %.3f %.3f' % (acc_test[-1], np.max(acc_test)))

                pltu.figure(model_name)
                plt.plot(acc_train)
                plt.plot(acc_test)
                plt.legend(('Train', 'Test'))
                plt.ylim(0.6, 1.0)
                pltu.show(show_grid=True)


def find_line(lines, pattern):
    return next(i for i, ln in enumerate(lines) if ln.startswith(pattern))


def reformat_logs(root_path):
    """Reformat old format of logs by putting model to the top."""

    for dir_path, _, file_names in os.walk(root_path):
        for file_name in file_names:
            if file_name == 'log.txt':
                file_path = os.path.join(dir_path, file_name)

                with open(file_path, mode='r') as file:
                    lines = file.readlines()

                model_ind = find_line(lines, 'Model:')
                output_ind = find_line(lines, 'Output:') + 1
                lines = lines[model_ind:output_ind] + lines[:model_ind] + lines[output_ind:]

                with open(file_path, mode='w') as file:
                    file.writelines(lines)


if __name__ == '__main__':
    # show_certh_mat('data/TrainRealGarmentDataSet.mat', slice(0, 15))
    # show_certh_mat('data/RealGarmentDataSetXY.mat', slice(0, 15))
    # show_certh_mat('data/Deep5ClassDemoData.mat', slice(0, 15))
    # show_certh_png('data/mariolis-train-rgbd/pants_006_000_001/pants_006_000_001_080.png')
    # show_corona('data/corona-real/Towel.npy', slice(0, 96))
    # show_certh_dps('data/doumanoglou-rgbd/shirts_001_001_001/shirts_001_001_001_001.dps')
    # show_poses('data/poses.txt')
    plot_accuracies('//home//striajan//fs//ptak//datagrid//knnnet//out-full')
    # reformat_logs('out')
