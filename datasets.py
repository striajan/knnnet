from __future__ import division, print_function
import h5py
import numpy as np
import os
import os.path
import torch
import torch.utils.data as data

import preprocessing


class PartDataset(data.Dataset):
    """Original dataset loader."""

    def __init__(self, root, npoints=2048, classification=False, class_choice=None, train=True):
        self.npoints = npoints
        self.root = root
        self.catfile = os.path.join(self.root, 'synsetoffset2category.txt')
        self.cat = {}

        self.classification = classification

        with open(self.catfile, 'r') as f:
            for line in f:
                ls = line.strip().split()
                self.cat[ls[0]] = ls[1]
        # print(self.cat)
        if class_choice is not None:
            self.cat = {k: v for k, v in self.cat.items() if k in class_choice}

        self.meta = {}
        for item in self.cat:
            # print('category', item)
            self.meta[item] = []
            dir_point = os.path.join(self.root, self.cat[item], 'points')
            dir_seg = os.path.join(self.root, self.cat[item], 'points_label')
            # print(dir_point, dir_seg)
            fns = sorted(os.listdir(dir_point))
            if train:
                fns = fns[:int(len(fns) * 0.9)]
            else:
                fns = fns[int(len(fns) * 0.9):]

            # print(os.path.basename(fns))
            for fn in fns:
                token = (os.path.splitext(os.path.basename(fn))[0])
                self.meta[item].append((os.path.join(dir_point, token + '.pts'),
                                        os.path.join(dir_seg, token + '.seg')))

        self.datapath = []
        for item in self.cat:
            for fn in self.meta[item]:
                self.datapath.append((item, fn[0], fn[1]))

        self.classes = dict(zip(self.cat, range(len(self.cat))))
        print(self.classes)
        self.num_seg_classes = 0
        if not self.classification:
            for i in range(len(self.datapath) // 50):
                l = len(np.unique(np.loadtxt(self.datapath[i][-1]).astype(np.uint8)))
                if l > self.num_seg_classes:
                    self.num_seg_classes = l
                    # print(self.num_seg_classes)

    def __getitem__(self, index):
        fn = self.datapath[index]
        cls = self.classes[self.datapath[index][0]]
        point_set = np.loadtxt(fn[1]).astype(np.float32)
        seg = np.loadtxt(fn[2]).astype(np.int64)
        # print(point_set.shape, seg.shape)

        choice = np.random.choice(len(seg), self.npoints, replace=True)
        # resample
        point_set = point_set[choice, :]
        seg = seg[choice]
        point_set = torch.from_numpy(point_set)
        seg = torch.from_numpy(seg)
        cls = torch.from_numpy(np.array([cls]).astype(np.int64))
        if self.classification:
            return point_set, cls
        else:
            return point_set, seg

    def __len__(self):
        return len(self.datapath)


class ArrayDataset(data.Dataset):
    """Dataset storing data in NumPy arrays.

    :type x: numpy.ndarray
    :type y: numpy.ndarray
    """

    def __init__(self):
        super(ArrayDataset, self).__init__()

        self.x = None
        self.y = None
        self.classes = None

    def __getitem__(self, i):
        return self.x[i], self.y[i:i+1]

    def __len__(self):
        return self.x.shape[0]

    def process(self, num_pts=0, normalize=True, normals=False):
        if normals:
            # 3D point + 3D normal
            self.x = self.x[:, :, :6]
        else:
            # 3D point only
            self.x = self.x[:, :, :3]

        # sample and permute points
        if num_pts > 0:
            self.x = preprocessing.sample(self.x, num_pts)

        # normalize to a zero centered unit ball
        if normalize:
            if normals and self.x.shape[2] == 6:
                self.x = preprocessing.normalize_with_normals(self.x)
            else:
                self.x = preprocessing.normalize(self.x)


class NumPyDataset(ArrayDataset):
    """Dataset loaded from NumPy file.

    :type x: numpy.ndarray
    :type y: numpy.ndarray
    :type classes: numpy.ndarray
    """

    def __init__(self, file_path, num_pts=0, normalize=True, normals=False):
        super(NumPyDataset, self).__init__()

        self.data = np.load(file_path, allow_pickle=True)
        self.x = self.data['x']
        self.y = self.data['y']
        self.classes = self.data['classes']

        self.process(num_pts, normalize, normals)


class Hdf5Dataset(ArrayDataset):
    """Dataset loaded from HDF5 files.

    :type x: numpy.ndarray
    :type y: numpy.ndarray
    :type classes: numpy.ndarray
    """

    def __init__(self, file_list, classes_path=None, num_pts=0, normalize=True, normals=False):
        super(Hdf5Dataset, self).__init__()

        self.x = list()
        self.y = list()

        directory = os.path.dirname(file_list)
        for line in open(file_list):
            file_name = os.path.basename(line.strip())
            data = h5py.File(os.path.join(directory, file_name))

            x = np.array(data['data'], dtype=np.float32)
            if normals:
                n = np.array(data['normal'], dtype=np.float32)
                x = np.concatenate((x, n), axis=2)
            self.x.append(x)

            self.y.append(np.squeeze(data['label']).astype(int))

        self.x = np.concatenate(self.x, axis=0)
        self.y = np.concatenate(self.y, axis=0)

        if classes_path is not None:
            self.classes = np.loadtxt(classes_path, dtype=str)

        self.process(num_pts, normalize, normals)


def main():
    print('Part chair:')
    d = PartDataset('data/shapenetcore', npoints=2048, class_choice=['Chair'])
    print(len(d))
    ps, seg = d[0]
    print(ps.size(), ps.type(), seg.size(), seg.type())

    print('Part:')
    d = PartDataset('data/shapenetcore', npoints=2048, classification=True)
    print(len(d))
    ps, cls = d[0]
    print(ps.size(), ps.type(), cls.size(), cls.type())

    print('ShapeNet without normals:')
    d = NumPyDataset('data/shapenetcore-normal.npz', num_pts=2048, normalize=True)
    print(len(d))
    ps, cls = d[0]
    print(ps.shape, ps.dtype, cls.shape, cls.dtype)

    print('ShapeNet with normals:')
    d = NumPyDataset('data/shapenetcore-normal.npz', num_pts=1024, normalize=True, normals=True)
    print(len(d))
    ps, cls = d[0]
    print(ps.shape, ps.dtype, cls.shape, cls.dtype)

    print('ModelNet training without normals:')
    d = Hdf5Dataset('data/modelnet_hdf5/train_files.txt', 'data/modelnet_hdf5/shape_names.txt',
                    num_pts=1024, normalize=True, normals=False)
    print(d.classes)
    print(len(d))
    ps, cls = d[0]
    print(ps.shape, ps.dtype, cls.shape, cls.dtype)

    print('ModelNet testing with normals:')
    d = Hdf5Dataset('data/modelnet_hdf5/test_files.txt', 'data/modelnet_hdf5/shape_names.txt',
                    num_pts=1024, normalize=True, normals=True)
    print(d.classes)
    print(len(d))
    ps, cls = d[0]
    print(ps.shape, ps.dtype, cls.shape, cls.dtype)


if __name__ == '__main__':
    main()
