import numpy as np


def sample(pts, num_pts):
    """Sample b*n*d points randomly.

    :type pts: numpy.ndarray
    :type num_pts: int
    :rtype: numpy.ndarray
    """
    if pts.shape[1] < num_pts:
        raise ValueError('Cannot sample %d out of %d points.' % (num_pts, pts.shape[1]))
    elif pts.shape[1] > num_pts:
        ind = np.random.choice(pts.shape[1], num_pts, replace=False).astype(int)
        pts = pts[:, ind]
    return pts


def normalize(pts, method='ball'):
    """Normalize b*n*3 points by making them centered to zero and either located in unit
    ball or having unit variance.

    :type pts: numpy.ndarray
    :type method: str
    :rtype: numpy.ndarray
    """

    # make the points zero centered
    mean = pts.mean(axis=1)
    pts -= mean[:, np.newaxis, :]

    if method == 'ball':
        # scale is the maximum distance over all points in the cloud
        dists = np.linalg.norm(pts, axis=2)
        scales = dists.max(axis=1)
    elif method == 'variance':
        # scale is the standard deviation over all axes
        b, n, d = pts.shape
        scales = pts.reshape((b, n * d)).std(axis=1)
    else:
        raise ValueError('Unknown normalization method: %s' % method)

    pts /= scales[:, np.newaxis, np.newaxis]

    return pts


def normalize_with_normals(pts_normals):
    """Normalize b*n*(3+3) points by making them centered to zero and located in unit ball
    and the normals by setting their norm to one.

    :type pts_normals: numpy.ndarray
    :rtype: numpy.ndarray
    """

    # make points zero centered and inside unit ball
    pts = pts_normals[:, :, 0:3]
    pts = normalize(pts)

    # make normals to have norm equal to one
    normals = pts_normals[:, :, 3:6]
    norms = np.linalg.norm(normals, axis=2)
    normals /= norms[:, :, np.newaxis]

    return np.concatenate((pts, normals), axis=2)


def main():
    pts_normals = [[1, 1, 1, 1, 0, 0], [2, 2, 2, 0, 2, 0], [3, 3, 3, 0, 3, 3], [4, 4, 4, 4, 4, 4]]
    pts_normals = np.tile(np.array(pts_normals, dtype=float)[np.newaxis], (6, 1, 1))
    print('input:', pts_normals[0], sep='\n')

    print('sample:', sample(pts_normals, 2)[0], sep='\n')
    print('normalize:', normalize(pts_normals[:, :, :3])[0], sep='\n')
    print('normalize_with_normals:', normalize_with_normals(pts_normals)[0], sep='\n')


if __name__ == '__main__':
    main()
