from __future__ import print_function
import torch

import augmentation
import crossval
import knet
import kpars
import lr_scheduler
import training
import utils


batch_size = 32
num_points = 1024
num_classes = 55
num_epochs = 200
path_train = 'data/shapenet-r.npz'
path_test = 'data/shapenet-s.npz'
device = 'cuda'
normalize = True
normals = False
seed = 1

input_dim = 6 if normals else 3
params = kpars.KnnNetParams(num_points, conv_type='full', input_dim=input_dim)

lr = 1e-3
# lr_gen = crossval.LogUniform(low=5e-4, high=5e-3, base=10)
# lr_space = crossval.log_space(1e-2, 1e-3, num=3)

weight_decay = 0

augment = augmentation.Composed(
    augmentation.Sampling(num_points),
    augmentation.RotationY())

F = kpars.FullConvLayerParams
P = kpars.PairConvLayerParams
L = kpars.LinearLayerParams

conv = [
    [F(0,  1, 64),
     F(0,  4, 128),
     F(0,  8, 512, activation='')],
    [F(0,  4, 64),
     F(0,  4, 128),
     F(0,  4, 512, activation='')],
    [F(0,  4, 64),
     F(0,  4, 128),
     F(0,  8, 512, activation='')],
    [F(0,  4, 64),
     F(0,  8, 128),
     F(0, 16, 512, activation='')],
]

lin = [
    [L(256, batch_norm=True),
     L(128, batch_norm=True),
     L(num_classes, activation='logsoftmax')],
]

for c in conv:
    for l in lin:
        params.set_conv(c)
        params.set_lin(l)

        # model
        model = knet.KnnNet(params)

        # optimizer
        model_params = filter(lambda p: p.requires_grad, model.parameters())
        optimizer = torch.optim.Adam(model_params, lr=lr, weight_decay=weight_decay)

        lr_schedule = lr_scheduler.ConstantLR(optimizer)

        trainer = training.Trainer(
            params, model, optimizer, lr_schedule, num_epochs, batch_size, path_train, path_test,
            num_points, normalize, normals, augment, device, seed)

        # trainer.check()
        trainer.train()
