from __future__ import print_function
import numpy as np
import torch


def random_rotation(distribution='uniform', sigma=0.1):
    """Generate random rotation angle, either from uniform or normal distribution.
    Return the angle, its cosine and sinus.

    :type distribution: basestring
    :type sigma: float
    :rtype: (float, float, float)
    """
    if distribution == 'uniform':
        angle = 2 * np.pi * np.random.uniform()
    elif distribution == 'normal':
        angle = np.random.normal(scale=sigma)
    return angle, np.cos(angle), np.sin(angle)


def rotate(rot, x):
    """Rotate points with optional normals using the specified rotation matrix.

    :type x: torch.FloatTensor
    :rtype: torch.FloatTensor
    """
    xt = torch.matmul(rot, x[:, :3])
    if x.shape[1] == 6:
        norm = torch.matmul(rot, x[:, 3:])
        xt = torch.cat((xt, norm), dim=1)
    return xt


class Transform(object):
    """Interface for transform of points."""

    def __init__(self):
        super(Transform, self).__init__()

    def apply(self, x):
        """Apply transform on batch of b*d*n points.

        :type x: torch.FloatTensor
        :rtype: torch.FloatTensor
        """
        raise NotImplementedError()

    def __repr__(self):
        return self.__class__.__name__


class Composed(Transform):
    """Compose multiple transforms into sequence."""

    def __init__(self, *transforms):
        super(Composed, self).__init__()
        self.transforms = list(transforms)

    def apply(self, x):
        for t in self.transforms:
            x = t.apply(x)
        return x

    def __repr__(self):
        return '{}({})'.format(self.__class__.__name__,
                               ', '.join(repr(t) for t in self.transforms))


class Sampling(Transform):
    """Sample points randomly."""

    def __init__(self, num_samples):
        super(Sampling, self).__init__()
        self.num_samples = num_samples

    def apply(self, x):
        ind = np.random.choice(x.shape[2], self.num_samples, replace=False)
        return x[:, :, ind]

    def __repr__(self):
        return '{}(n={num_samples})'.format(self.__class__.__name__, **self.__dict__)


class Shuffling(Transform):
    """Shuffle points randomly."""

    def __init__(self):
        super(Shuffling, self).__init__()

    def apply(self, x):
        ind = np.random.permutation(x.shape[2])
        return x[:, :, ind]


class RotationY(Transform):
    """Rotate points randomly around the axis y."""

    def __init__(self):
        super(RotationY, self).__init__()

    def apply(self, x):
        # random rotation matrix
        _, c, s = random_rotation()
        rot = x.new_tensor([[c, 0, s], [0, 1, 0], [-s, 0, c]])

        return rotate(rot, x)


class RotationXyz(Transform):
    """Rotate points randomly around the axes x, y, z."""

    def __init__(self):
        super(RotationXyz, self).__init__()

    def apply(self, x):
        # matrices for rotations around the axes x, y, z
        _, c, s = random_rotation()
        rot_x = np.array([[1, 0, 0], [0, c, -s], [0, s, c]])
        _, c, s = random_rotation()
        rot_y = np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]])
        _, c, s = random_rotation()
        rot_z = np.array([[c, -s, 0], [s, c, 0], [0, 0, 1]])

        # general rotation matrix
        rot = np.dot(rot_z, np.dot(rot_y, rot_x))
        rot = x.new_tensor(rot)

        return rotate(rot, x)


class Jitter(Transform):
    """Jitter points with random normal noise."""

    def __init__(self, sigma=0.01, clip=None, normals=True):
        super(Jitter, self).__init__()
        self.sigma = sigma
        self.clip = clip
        self.normals = normals

    def jitter(self, data):
        noise = self.sigma * torch.randn_like(data)

        if self.clip is not None:
            noise.clamp_(-self.clip, self.clip)

        return data + noise

    def apply(self, x):
        # jitter points
        xt = self.jitter(x[:, :3])

        if x.shape[1] == 6:
            norm = x[:, 3:]

            # optionally jitter and re-normalize normals
            if self.normals:
                norm = self.jitter(norm)
                norm /= torch.norm(norm, dim=1, keepdim=True)

            xt = torch.cat((xt, norm), dim=1)

        return xt

    def __repr__(self):
        return '{}(sigma={sigma}, clip={clip}, normals={normals})'.format(
            self.__class__.__name__, **self.__dict__)


class Scaling(Transform):
    """Scale (already centered) points random uniformly."""

    def __init__(self, low=0.8, high=1.25):
        super(Scaling, self).__init__()
        self.low = low
        self.high = high

    def apply(self, x):
        scale = np.random.uniform(self.low, self.high)

        if x.shape[1] == 3:
            return x * scale
        else:
            # do not scale normals
            return torch.cat((scale * x[:, :3], x[:, 3:]), dim=1)

    def __repr__(self):
        return '{}(low={low}, high={high})'.format(self.__class__.__name__, **self.__dict__)


class Translation(Transform):
    """Translate points with random uniform shift."""

    def __init__(self, rng=0.1):
        super(Translation, self).__init__()
        self.rng = rng

    def apply(self, x):
        # same random translation for all points
        trans = torch.rand((1, 3, 1), dtype=x.dtype, device=x.device)
        trans = (2 * trans - 1) * self.rng

        if x.shape[1] == 3:
            return x + trans
        else:
            # do not translate normal vectors
            return torch.cat((x[:, :3] + trans, x[:, 3:]), dim=1)

    def __repr__(self):
        return '{}(rng={rng})'.format(self.__class__.__name__, **self.__dict__)


def augmentations(x):
    print('Input:', x[0], '', sep='\n')
    for transform in (Sampling(2), Shuffling(), RotationY(), RotationXyz(),
                      Jitter(), Scaling(), Translation()):
        xt = transform.apply(x)
        print(transform.__class__.__name__, xt[0], '', sep='\n')


def main():
    # points without normals
    x = np.array([[1, 2, 3, 4, 5, 6]])
    x = torch.tensor(x, dtype=torch.float32, device='cpu').repeat(4, 3, 1)
    augmentations(x)

    # points with normals
    x = np.array([[1, 1, 1, 1, 0, 0], [2, 2, 2, 0, 1, 0], [-1, -1, -1, 0, 0, 1]])
    x = torch.tensor(x.transpose(), dtype=torch.float32, device='cpu').repeat(4, 1, 1)
    augmentations(x)


if __name__ == '__main__':
    main()
