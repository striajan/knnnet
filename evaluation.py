from __future__ import division
import numpy as np
import sklearn.metrics
import sklearn.model_selection


def class_accuracy(f, y, num_classes=None):
    """Compute mean class accuracy for the classifications f and the expected classes y.

    :type f: numpy.ndarray
    :type y: numpy.ndarray
    :type num_classes: int
    :rtype: numpy.ndarray
    """

    if num_classes is None:
        num_classes = max(np.max(f), np.max(y)) + 1

    # correctly classified items per class and number of seen items per class
    correct = np.bincount(f[f == y], minlength=num_classes)
    seen = np.bincount(y, minlength=num_classes)

    acc = np.ones(num_classes, dtype=float)
    ind = seen > 0
    acc[ind] = correct[ind] / seen[ind]

    return acc


class Evaluator(object):
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.confusion = np.zeros((num_classes, num_classes), dtype=int)
        self.total_loss = 0.0
        self.num_batches = 0

    def batch(self, f, y, loss=0):
        # update confusion matrix
        confusion = sklearn.metrics.confusion_matrix(y, f, labels=range(self.num_classes))
        self.confusion += confusion

        # update loss
        self.total_loss += loss
        self.num_batches += 1

        # compute accuracy for current data
        accuracy = confusion.diagonal().sum() / confusion.sum()
        return accuracy

    @property
    def total_seen(self):
        return self.confusion.sum()

    @property
    def total_correct(self):
        return self.confusion.diagonal().sum()

    @property
    def mean_accuracy(self):
        return self.total_correct / self.total_seen

    @property
    def class_seen(self):
        return self.confusion.sum(axis=1)

    @property
    def class_correct(self):
        return self.confusion.diagonal()

    @property
    def class_accuracy(self):
        return self.class_correct / self.class_seen

    @property
    def mean_class_accuracy(self):
        return np.mean(self.class_accuracy)

    @property
    def relative_confusion(self):
        class_seen = np.tile(self.class_seen[:, np.newaxis], (1, self.num_classes))
        return self.confusion / class_seen

    @property
    def mean_loss(self):
        return self.total_loss / self.num_batches

    @property
    def confusion_str(self):
        code = ' \u2591\u2592\u2593\u2588'

        # remap relative confusion matrix to [0, 1]
        conf = self.relative_confusion
        conf = (conf - conf.min()) / (conf.max() - conf.min())

        # map confusion values to code indices
        n = len(code)
        ind = np.minimum((n * conf).astype(int), n - 1)

        return '\n'.join(''.join(code[val] for val in row) for row in ind)
