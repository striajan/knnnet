from __future__ import division, print_function
from matplotlib.widgets import Slider
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import scipy.stats

import plot_utils as pltu


def beta(a, b, n=100):
    x = np.linspace(0, 1, n)
    distr = scipy.stats.beta(a, b)
    p = distr.pdf(x)
    return x, p


def interactive_beta(a=1, b=1):
    pltu.figure('Beta distribution')
    plt.subplots_adjust(left=0.1, bottom=0.2)

    axis = plt.gca()
    axis.set_ylim(0, 10)

    x, p = beta(a, b)
    line = plt.plot(x, p)[0]

    slider_a = Slider(plt.axes([0.1, 0.05, 0.35, 0.05]), 'a', 1, 10, a)
    slider_b = Slider(plt.axes([0.6, 0.05, 0.35, 0.05]), 'b', 1, 10, b)

    def slider_update(_):
        x, p = beta(slider_a.val, slider_b.val)
        line.set_ydata(p)

    slider_a.on_changed(slider_update)
    slider_b.on_changed(slider_update)

    pltu.show()


def beta_2d(a1, b1, a2, b2, n1, n2):
    x1, p1 = beta(a1, b1, n1)
    x2, p2 = beta(a2, b2, n2)
    return p1[np.newaxis, :] * p2[:, np.newaxis]


def interactive_beta_2d(a1=1, b1=1, a2=1, b2=1, n1=17, n2=20, mode='circle'):
    fig = pltu.figure('2D beta distribution')
    plt.subplots_adjust(left=0.1, bottom=0.15)

    axis = plt.gca()
    axis.set_ylim(0, 10)

    if mode == 'circle':
        phi = np.linspace(-np.pi / 2, np.pi / 2, n1)
        r = np.linspace(0, 1, n2)
        phi, r = np.meshgrid(phi, r)
        x = r * np.cos(phi)
        y = r * np.sin(phi)

    elif mode == 'square':
        x = np.linspace(0, 1, n1)
        y = np.linspace(0, 1, n2)
        x, y = np.meshgrid(x, y)

    p = beta_2d(a1, b1, a2, b2, n1, n2)

    axis = fig.gca(projection='3d')
    axis.plot_wireframe(x, y, p, rstride=1, cstride=1)

    slider_a1 = Slider(plt.axes([0.1, 0.05, 0.35, 0.03]), '$a_1$', 1, 10, a1)
    slider_b1 = Slider(plt.axes([0.1, 0.01, 0.35, 0.03]), '$b_1$', 1, 10, b1)
    slider_a2 = Slider(plt.axes([0.6, 0.05, 0.35, 0.03]), '$a_2$', 1, 10, a2)
    slider_b2 = Slider(plt.axes([0.6, 0.01, 0.35, 0.03]), '$b_2$', 1, 10, b2)

    def slider_update(_):
        p = beta_2d(slider_a1.val, slider_b1.val, slider_a2.val, slider_b2.val, n1, n2)
        axis.clear()
        axis.plot_wireframe(x, y, p, rstride=1, cstride=1)

    for slider in (slider_a1, slider_b1, slider_a2, slider_b2):
        slider.on_changed(slider_update)

    pltu.show()


def split(n, d):
    if d == 1:
        # nothing left for splitting
        yield (n,)
    else:
        for i in range(1, n - d + 2):
            for s in split(n - i, d - 1):
                yield (i,) + s


def dirichlet(a, n):
    # find combinations s[i, 0] + s[i, 1] + ... + s[i, d] = n s.t. s[i, j] >= 1
    d = len(a)
    s = np.array(list(split(n, d)))

    # compute Dirichlet probabilities
    distr = scipy.stats.dirichlet(a)
    p = np.zeros(s.shape[0])
    for i, si in enumerate(s):
        p[i] = distr.pdf(si / n)

    return s, p


def interactive_dirichlet(a1, a2, a3, n=20):
    x = y = np.linspace(0, 1, n)
    x, y = np.meshgrid(x, y)

    s, p = dirichlet((a1, a2, a3), n)

    z = np.zeros((n, n))
    z[s[:, 0], s[:, 1]] = p

    fig = pltu.figure('Dirichlet distribution')
    axis = fig.gca(projection='3d')
    axis.plot_wireframe(x, y, z, rstride=1, cstride=1)

    slider_a1 = Slider(plt.axes([0.1, 0.01, 0.2, 0.03]), '$a_1$', 1, 10, a1)
    slider_a2 = Slider(plt.axes([0.4, 0.01, 0.2, 0.03]), '$a_2$', 1, 10, a2)
    slider_a3 = Slider(plt.axes([0.7, 0.01, 0.2, 0.03]), '$a_3$', 1, 10, a3)

    def slider_update(_):
        s, p = dirichlet((slider_a1.val, slider_a2.val, slider_a3.val), n)
        z[:] = 0
        z[s[:, 0], s[:, 1]] = p
        axis.clear()
        axis.plot_wireframe(x, y, z, rstride=1, cstride=1)

    for slider in (slider_a1, slider_a2, slider_a3):
        slider.on_changed(slider_update)

    pltu.show()


if __name__ == '__main__':
    # interactive_beta()
    # interactive_beta_2d(2, 3, 2, 2)
    interactive_dirichlet(2, 3, 2)
