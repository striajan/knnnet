#!/bin/bash

nvidia-smi

echo "Select GPU:"
read GPU_ID

if [ -n "$GPU_ID" ]; then
	# use the specified GPU in Theano
	export THEANO_FLAGS=device=gpu${GPU_ID}

	# use the specified GPU in TensorFlow
	export CUDA_DEVICE_ORDER=PCI_BUS_ID
	export CUDA_VISIBLE_DEVICES=${GPU_ID}
fi

# settings
export TF_CPP_MIN_LOG_LEVEL=2

echo "Running: $@"

python -O "$@"
