from __future__ import print_function
import numpy as np
import sklearn.model_selection
import sklearn.svm
import termcolor

from crossval import LogUniform, log_space
from evaluation import Evaluator
from watch import Watch


def load_data(path):
    """Load data from file and decode bytes to strings."""

    data = dict(np.load(path))

    data['classes'] = data['classes'].astype(np.str)
    data['names'] = data['names'].astype(np.str)

    return data


def split_names(pcd_names):
    """Split and group PCD file names by category and item."""

    split = dict()

    for name in pcd_names:
        category, item = name.split('_')[0:2]
        if category not in split:
            split[category] = dict()
        if item not in split[category]:
            split[category][item] = list()
        split[category][item].append(name)

    return split


def leave_one_out_split(x, y, names, per_category):
    """Create split, leaving one item out."""

    split = split_names(names)
    groups = dict()

    # split names to groups
    group_id = 1
    for category in split:
        if per_category:
            group_id = 1
        for item in split[category]:
            for file_name in split[category][item]:
                groups[file_name] = group_id
            group_id += 1

    # create group vector in the original ordering of names
    groups = [groups[name] for name in names]

    # use splitter from scikit-learn
    selection = sklearn.model_selection.LeaveOneGroupOut()
    split = selection.split(x, y, groups)

    return split


def train_split(x, y, names, eval, num_iter=56, c_rng=(0.01, 1.0), strategy='grid'):
    """Train the SVM classifier. Parameters are found by cross-validation.

    :type x: numpy.ndarray
    :type y: numpy.ndarray
    :type groups: numpy.ndarray
    :type eval: evaluation.Evaluator
    :type num_iter: int
    :type c_rng: (float, float)
    :type strategy: str
    :rtype: sklearn.base.BaseEstimator
    """

    print('Train classes:', np.bincount(y))

    # SVM estimator using libsvm (SVC) or liblinear (LinearSVC)
    # svm = sklearn.svm.SVC()
    svm = sklearn.svm.LinearSVC()

    # cross-validation split (k-fold or leave one out)
    if names is None:
        split = sklearn.model_selection.StratifiedKFold(n_splits=3, shuffle=False, random_state=1)
    else:
        split = leave_one_out_split(x, y, names, per_category=True)

    if strategy == 'grid':
        # optimize over complete grid of hyper-params
        param_grid = {'C': log_space(c_rng[0], c_rng[1], num_iter)}
        search = sklearn.model_selection.GridSearchCV(
            svm, param_grid, n_jobs=num_iter, cv=split)
    elif strategy == 'random':
        # optimize over randomly generated hyper-params
        param_distribution = {'C': LogUniform(c_rng[0], c_rng[1])}
        search = sklearn.model_selection.RandomizedSearchCV(
            svm, param_distribution, n_iter=num_iter, n_jobs=num_iter, cv=split)
    else:
        raise ValueError('Unknown strategy (must be random or grid): %s' % strategy)

    with Watch('Optimization (%d)' % num_iter):
        search.fit(x, y)

    print('Best params:', search.best_params_)

    # retrain the classifier on all data using the optimum params
    svm = search.best_estimator_
    svm.fit(x, y)
    f = svm.predict(x)

    if eval is not None:
        accuracy = eval.batch(f, y)
        print('Train accuracy: %.5f' % accuracy)

    return svm


def test_split(classifier, x, y, eval=None):
    """Evaluate testing accuracy.

    :type classifier: sklearn.svm.LinearSVC
    :type x: numpy.ndarray
    :type y: numpy.ndarray
    :type eval: evaluation.Evaluator
    """

    print('Classes:', np.bincount(y))

    # use the optimized model for prediction
    f = classifier.predict(x)

    if eval is not None:
        acc_test = eval.batch(f, y)
        print('Test accuracy: %.5f' % acc_test)


def print_evaluation(name, eval, color=None):
    """Print evaluation using the specified color.

    :type name: str
    :type eval: evaluation.Evaluator
    :type color: str
    """
    termcolor.cprint(name, color)
    termcolor.cprint('Mean accuracy: %.5f' % eval.mean_accuracy, color)
    termcolor.cprint('Class accuracy: %s' % eval.class_accuracy, color)
    termcolor.cprint('Mean class accuracy: %.5f' % eval.mean_class_accuracy, color)
    termcolor.cprint('Confusion matrix:\n%s' % eval.relative_confusion, color)


def evaluate_leave_one_out_splits(data, per_category=True):
    """Evaluate using leave one out method."""

    x = data['f']
    y = data['y']
    names = data['names']
    num_classes = max(y) + 1

    eval_train = Evaluator(num_classes)
    eval_test = Evaluator(num_classes)

    split = leave_one_out_split(x, y, names, per_category=True)

    for s, (ind_train, ind_test) in enumerate(split):
        print('SPLIT %d' % (s + 1))
        classifier = train_split(x[ind_train], y[ind_train], names[ind_train], eval_train)
        test_split(classifier, x[ind_test], y[ind_test], eval_test)

    print_evaluation('TRAIN', eval_train, 'blue')
    print_evaluation('TEST', eval_test, 'green')
    print('Classes:', data['classes'])


def filter_data_classes(data, classes):
    """Relabel and filter data so that only the defined classes remain."""

    y_data = data['y']
    y_filt = np.full(len(y_data), -1)

    # rename classes
    for class_id, class_name in enumerate(classes):
        c = np.where(data['classes'] == class_name)[0][0]
        y_filt[y_data == c] = class_id

    # keep only the defined classes
    ind = y_filt >= 0
    f = data['f'][ind]
    y_filt = y_filt[ind]

    return f, y_filt


def evaluate_fixed_split(data_train, data_test):
    """Evaluate on the specified training and testing sets."""

    # classes appearing in both datasets
    classes = list(set(data_train['classes']) & set(data_test['classes']))
    num_classes = len(classes)

    # filter datasets to contain equally labeled data from same classes
    x_train, y_train = filter_data_classes(data_train, classes)
    x_test, y_test = filter_data_classes(data_test, classes)

    eval_train = Evaluator(num_classes)
    eval_test = Evaluator(num_classes)

    # evaluate_split(x_train, y_train, x_test, y_test, eval_train, eval_test)

    print_evaluation('TRAIN', eval_train, 'blue')
    print_evaluation('TEST', eval_test, 'green')


def evaluate_random_split(data):
    """Evaluate on random split of data to training and testing set."""

    x = data['f']
    y = data['y']
    num_classes = max(y) + 1

    print('Input:', x.shape)

    eval_train = Evaluator(num_classes)
    eval_test = Evaluator(num_classes)

    # random split with the specified size of the test set
    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(x, y, test_size=0.1)

    # evaluate and test
    classifier = train_split(x_train, y_train, None, eval_train)
    test_split(classifier, x_test, y_test, eval_test)

    print_evaluation('TRAIN', eval_train, 'blue')
    print_evaluation('TEST', eval_test, 'green')


def main():
    np.set_printoptions(precision=5)

    # leave one out
    data = load_data('data/doumanoglou-448.npz')
    evaluate_leave_one_out_splits(data)

    # # fixed train/test split
    # data_train = load_data('data/mariolis-train_feat.npz')
    # data_test = load_data('data/mariolis-test_feat.npz')
    # evaluate_fixed_split(data_train, data_test)

    # # random split
    # data = load_data('data/corona.npz')
    # evaluate_random_split(data)


if __name__ == '__main__':
    main()
