from __future__ import print_function
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F


def transform(t, x):
    """Apply c*c transform matrix on batch of b*c*n inputs.

    :type t: torch.Tensor
    :type x: torch.Tensor
    :rtype: torch.Tensor
    """
    # b*c*n (channels first) -> b*n*c
    x = x.transpose(1, 2)
    x = torch.bmm(x, t)
    x = x.transpose(1, 2)
    return x


class TransMat(nn.Module):
    def __init__(self, mat):
        super(TransMat, self).__init__()
        self.mat = nn.Parameter(torch.FloatTensor(mat), requires_grad=False)
        self.register_parameter('mat', self.mat)

    def forward(self, x):
        batch_size = x.size()[0]
        t = self.mat.unsqueeze(0).repeat(batch_size, 1, 1)
        return t


class TransNet(nn.Module):
    def __init__(self, num_points=1024, point_dim=3):
        super(TransNet, self).__init__()

        self.num_points = num_points
        self.point_dim = point_dim

        self.conv1 = torch.nn.Conv1d(point_dim, 64, 1)
        self.conv2 = torch.nn.Conv1d(64, 128, 1)
        self.conv3 = torch.nn.Conv1d(128, 1024, 1)
        self.mp1 = torch.nn.MaxPool1d(num_points)
        self.fc1 = nn.Linear(1024, 512)
        self.fc2 = nn.Linear(512, 256)
        self.fc3 = nn.Linear(256, point_dim * point_dim)
        self.relu = nn.ReLU()
        self.bn1 = nn.BatchNorm1d(64)
        self.bn2 = nn.BatchNorm1d(128)
        self.bn3 = nn.BatchNorm1d(1024)
        self.bn4 = nn.BatchNorm1d(512)
        self.bn5 = nn.BatchNorm1d(256)

        self.eye = nn.Parameter(torch.eye(point_dim), requires_grad=False)
        self.register_parameter('eye', self.eye)

    def forward(self, x):
        # d*n -> 64*n -> 128*n -> 1024*n
        x = F.relu(self.bn1(self.conv1(x)))
        x = F.relu(self.bn2(self.conv2(x)))
        x = F.relu(self.bn3(self.conv3(x)))

        # max-pool over n points -> 1024 global features
        x = self.mp1(x)
        x = x.view(-1, 1024)

        # 1024 -> 512 -> 256 -> d*d
        x = F.relu(self.bn4(self.fc1(x)))
        x = F.relu(self.bn5(self.fc2(x)))
        x = self.fc3(x)

        # d*d vector -> d*d matrix -> add ones to diagonal
        x = x.view(-1, self.point_dim, self.point_dim)
        x = x + self.eye

        return x


def trans_loss(trans):
    """The transformation loss makes the transformation matrices orthogonal
    by minimizing ||T * T' - I||^2.

    :type trans: torch.Tensor
    :rtype: torch.Tensor
    """
    batch_size = trans.shape[0]

    # T * T'
    trans2 = torch.bmm(trans, trans.permute(0, 2, 1))

    # I
    eye = torch.eye(3, dtype=trans.dtype, device=trans.device)
    eye = eye.view(1, 3, 3).repeat(batch_size, 1, 1)

    # ||T * T' - I||^2
    return F.mse_loss(trans2, eye)


def main():
    num_pts = 2048
    x = torch.rand(32, 3, num_pts)

    trans = TransMat(np.eye(3))
    t = trans(x)
    f = transform(t, x)
    print('TransMat', t.size(), f.size())

    trans = TransNet(num_pts, point_dim=3)
    t = trans(x)
    f = transform(t, x)
    print('TransNet', t.size(), f.size())

    loss = trans_loss(t)
    loss.backward()
    print('Loss', loss.item())


if __name__ == '__main__':
    main()
