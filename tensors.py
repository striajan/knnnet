from __future__ import print_function
import torch
import torch.nn as nn


class BatchIndex(object):
    """Repeated index over batch items.

    :type ind: torch.Tensor
    """

    def __init__(self):
        super(BatchIndex, self).__init__()

        self.ind = None
        self.b = 0
        self.rep = 0

    def get(self, x, rep):
        b = x.shape[0]

        if self.ind is None or self.ind.device != x.device or self.b != b or self.rep != rep:
            self.ind = torch.arange(b, dtype=torch.long, device=x.device)
            self.ind = self.ind.unsqueeze(1).repeat(1, rep).view(b * rep)
            self.b = b
            self.rep = rep

        return self.ind


def activation_module(activation):
    """Create activation layer based on its name.

    :type activation: str
    :rtype: torch.nn.Module
    """
    activation = activation.lower()
    if activation == 'relu':
        return nn.ReLU()
    elif activation == 'logsoftmax':
        return nn.LogSoftmax(dim=1)
    else:
        return None


def activation_name(activation):
    """Get standardized name of the activation layer.

    :type activation: torch.nn.Module
    :rtype: str
    """
    activation_names = {nn.ReLU: 'relu', nn.LeakyReLU: 'leaky_relu', nn.Tanh: 'tanh'}
    if (activation is not None) and (type(activation) in activation_names.keys()):
        return activation_names[type(activation)]
    return 'linear'


def main():
    x = torch.rand(16, 8, 32)
    index = BatchIndex()
    print(index.get(x, 4))


if __name__ == '__main__':
    main()
