from __future__ import division, print_function
import numpy as np
import os
import shutil
import skimage.io

import plot_utils as pltu


def process_rgbd_sequence(src_dir, dst_dir, top, bottom, left, right, min_depth, max_depth):
    """Process sequence of RGB images and depth maps, cropping them and colorizing depths."""

    import cv2

    for file in sorted(os.listdir(src_dir)):
        src_path = os.path.join(src_dir, file)
        if file.endswith('.jpg') or file.endswith('.png'):
            print('Processing', file)
            img = skimage.io.imread(src_path)

            # crop
            img = img[top:bottom, left:right]

            if file.endswith('.png'):
                mask = np.logical_and(min_depth <= img, img <= max_depth)

                # convert from 16-bit to 8-bit
                img = (img - min_depth) / (max_depth - min_depth)
                img = np.asarray(255 * img, dtype=np.uint8)

                # colorize
                img = cv2.applyColorMap(img, cv2.COLORMAP_PARULA)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                img[~mask] = 255

            dst_path = os.path.join(dst_dir, file.split('_')[-1])
            skimage.io.imsave(dst_path, img)


def escape_accents(input_path, output_path):
    """Replace Czech accented letters by LaTeX escape sequences."""

    accents = {
        'ě': '\\v{e}',
        'š': '\\v{s}',
        'č': '\\v{c}',
        'ř': '\\v{r}',
        'ž': '\\v{z}',
        'ň': '\\v{n}',
        'ď': "\\hbox{d\\kern-1.5pt'}",
        'ť': "\\hbox{t\\kern-1pt'}",
        'Š': '\\v{S}',
        'Č': '\\v{C}',
        'Ř': '\\v{R}',
        'Ž': '\\v{Z}',
        'Ň': '\\v{N}',
        'Ď': '\\v{D}',
        'Ť': '\\v{T}',
        'á': "\\'{a}",
        'é': "\\'{e}",
        'í': "\\'{i}",
        'ó': "\\'{o}",
        'ú': "\\'{u}",
        'ý': "\\'{y}",
        'Á': "\\'{A}",
        'É': "\\'{E}",
        'Í': "\\'{I}",
        'Ó': "\\'{O}",
        'Ú': "\\'{U}",
        'Ý': "\\'{Y}",
        'ů': "\\r{u}"
    }

    with open(input_path, 'rt') as f:
        text = f.read()

    for letter, escape in accents.items():
        text = text.replace(letter, escape)

    with open(output_path, 'wt') as f:
        f.write(text)


def extract_log(logs, model_id):
    """Find log for particular model."""

    # split multiple logs
    indices = [i for i, line in enumerate(logs) if line == 'Model:\n'] + [len(logs)]

    if indices[0] != 0:
        raise ValueError('Each log is expected to start with model description.')

    if len(indices) == 2:
        print('Model ' + model_id + ' is located in separate log')
        return logs

    # find log for model
    for i in range(len(indices) - 1):
        log = logs[indices[i]:indices[i + 1]]
        for line in log:
            if line.startswith('Output: '):
                output = line.strip().split(' ')[1]
                if os.path.split(output)[-1] == model_id:
                    print('Model ' + model_id + ' was separated from multi-log')
                    return log

    raise ValueError('Model not found in log.')


def split_logs(root_path):
    """Split files containing logs for multiple models."""

    for dir_path, _, file_names in os.walk(root_path):
        for file_name in file_names:
            if file_name == 'log.txt':
                model_id = os.path.split(dir_path)[-1]
                log_path = os.path.join(dir_path, file_name)

                # read log
                with open(log_path, 'rt') as f:
                    log = f.readlines()

                model_log = extract_log(log, model_id)

                # if file contained more than one log, back it up and save extracted log
                if model_log != log:
                    shutil.copyfile(log_path, os.path.join(dir_path, 'log_full.txt'))
                    with open(log_path, 'wt') as f:
                        f.writelines(model_log)


if __name__ == '__main__':
    # process_rgbd_sequence('data/fusion_in', 'data/fusion_out',
    #                       top=0, bottom=570, left=140, right=340,
    #                       min_depth=950, max_depth=1225)

    # escape_accents('data/input.txt', 'data/output.txt')

    split_logs('out')
